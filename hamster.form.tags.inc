<?php


function hamster_tags_form(array $form_state, array $filter_params = array()) {

  $form = array();

  $form['items'] = array();

  $form['multicrud'] = _hamster_tags_multicrud_element($filter_params);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
    '#attributes' => array('tabindex' => 1),
  );

  $form['#filter_params'] = $filter_params;

  // generic submit handler provided by multicrud core.
  $form['#submit'] = array('multicrud_submit_all');

  return $form;
}


function _hamster_tags_multicrud_element(array $filter_params) {

  $element = array(
    '#type' => 'multicrud',

    // form elements for each row
    '#multicrud_elements' => _hamster_tags_multicrud_row_elements($filter_params),

    // value handler to load and save
    '#multicrud_valueHandler' => _hamster_tags_multicrud_value_handler($filter_params),

    // disable tabledrag and hierarchical behavior.
    '#multicrud_weights' => FALSE,
    '#multicrud_parents' => FALSE,
    '#multicrud_insert' => FALSE,
    '#multicrud_delete' => FALSE,
  );

  return $element;
}


function _hamster_tags_multicrud_row_elements(array $filter_params) {

  $elements = array();
  $elements['tag'] = array(
    '#title' => 'Hamster tag',
    '#input' => TRUE,
  );
  $elements['duration'] = array(
    '#title' => 'Duration',
    '#input' => TRUE,
  );
  $elements['latest'] = array(
    '#title' => 'Latest',
    '#input' => TRUE,
  );
  $elements['n_facts'] = array(
    '#title' => '#',
    '#input' => TRUE,
  );
  $elements['associated_nid'] = array(
    '#title' => 'Associated Drupal node',
    '#type' => 'textfield',
    '#size' => 22,
    '#autocomplete_path' => 'hamster/autocomplete/tag',
  );

  return $elements;
}


/**
 * Factory function for the value handler
 */
function _hamster_tags_multicrud_value_handler($filter_params) {
  return new _hamster_tags_MulticrudValueHandler($filter_params);
}


/**
 * The value handler is responsible for loading, saving and validation of items.
 */
class _hamster_tags_MulticrudValueHandler extends multicrud_ValueHandler {

  protected $_filterParams;
  protected $_originalItems;
  protected $_linkOptions;

  /**
   * @param $type
   *   the node type
   * @param $langSpecificFields
   *   names of fields that are not shared across languages
   */
  function __construct(array $filter_params) {
    $this->_filterParams = $filter_params;
    $this->_originalItems = _hamster_tags_multicrud_default_value($filter_params);
    $this->_linkOptions = array();
    foreach (array('date_begin', 'date_end') as $key) {
      if (!empty($filter_params[$key])) {
        $this->_linkOptions['query'][$key] = $filter_params[$key];
      }
    }
    module_load_include('inc', 'hamster', 'hamster.db');
  }

  function validateItem($item, $meta, $error_handler) {
    // TODO: Implement item validation
  }

  function getDefaultValue() {
    $items = $this->_originalItems;
    foreach ($items as $k => &$item) {
      $path = 'hamster/' . $item['hamster_source_id'] . '/tags/' . $item['id'];
      $item['tag'] = l($item['name'] . '@' . $item['category_name'], $path, $this->_linkOptions);
      if ($item['n_facts']) {
        $item['duration'] = number_format($item['duration'] / 60. / 60, 2);  // . ' h';
        $item['latest'] = date('Y-m-d', $item['latest']);
      }
      else {
        $item['duration'] = '-';
        $item['latest'] = '-';
      }
      if ($item['associated_nid']) {
        $item['associated_nid'] = $item['node_title'] . ' [nid:' . $item['associated_nid'] . ']';
      }
    }
    return $items;
  }

  /**
   * Called from multicrud for to-be-updated items.
   */
  function updateItem($id, $item, $meta) {
    if (!$this->_validateItem($item)) {
      return FALSE;
    }
    if (!isset($this->_originalItems[$id])) {
      // we cannot update something that does not exist.
      return;
    }
    $original_item = $this->_originalItems[$id];
    if (preg_match('#^[1-9][0-9]*$#', trim($item['associated_nid']), $m)) {
      $original_item['associated_nid'] = (int)$m[0];
    }
    else if (preg_match('#\[nid: *([1-9][0-9]*)\]$#', trim($item['associated_nid']), $m)) {
      $original_item['associated_nid'] = (int)$m[1];
    }
    else if (trim($item['associated_nid']) === '') {
      $original_item['associated_nid'] = NULL;
    }
    $success = _hamster_write_record('hamster_tags', $original_item, array('id', 'hamster_source_id'));
    return $success;
  }

  protected function _validateItem($item) {
    return TRUE;
  }
}


function _hamster_tags_multicrud_default_value(array $filter_params) {

  $hsid = 'hamster_source_id';

  $select_sum = db_select('hamster_fact_tags', 'ft');
  $select_sum->fields('ft', array($hsid, 'tag_id'));
  $select_sum->addExpression('COUNT(ft.tag_id)', 'n_facts');
  $select_sum->addExpression('SUM(f.end_time - f.start_time)', 'duration');
  $select_sum->addExpression('MAX(f.end_time)', 'latest');
  $select_sum->addExpression("COUNT(f.id)", 'n_facts');
  $select_sum->leftJoin('hamster_facts', 'f', "ft.fact_id = f.id  AND  ft.$hsid = f.$hsid");

  if (!empty($filter_params[$hsid])) {
    $select_sum->condition("ft.$hsid", $filter_params[$hsid]);
    if (!empty($filter_params['fact_id'])) {
      $select_sum->condition('ft.fact_id', $filter_params['fact_id']);
    }
    else if (!empty($filter_params['activity_id'])) {
      $select_sum->condition('f.activity_id', $filter_params['activity_id']);
    }
    else if (!empty($filter_params['category_id'])) {
      $select_sum->leftJoin('hamster_activities', 'a', "f.activity_id = a.id  AND  f.$hsid = a.$hsid");
      $select_sum->condition("a.category_id", $filter_params['category_id']);
    }
  }
  else if (!empty($filter_params['uid'])) {
    $select_sum->addField('s', 'uid');
    $select_sum->leftJoin('hamster_sources', 's', "ft.$hsid = s.id");
    $select_sum->condition('s.uid', $filter_params['uid']);
  }

  $select_sum->groupBy('ft.tag_id');
  $select_sum->groupBy("ft.$hsid");

  $select = db_select('hamster_tags', 't');
  $select->fields('t');
  $select->addField('n', 'title', 'node_title');
  $select->fields('sum', array('duration', 'n_facts', 'latest'));
  $select->leftJoin('node', 'n', 'n.nid = t.associated_nid');
  $select->leftJoin($select_sum, 'sum', "sum.tag_id = t.id  AND  sum.$hsid = t.$hsid");
  $select->orderBy('sum.latest', 'ASC');

  if (!empty($filter_params[$hsid])) {
    $select->condition("t.$hsid", $filter_params[$hsid]);
    if (!empty($filter_params['category_id'])) {
      $select->condition('sum.category_id', $filter_params['category_id']);
    }
  }
  else if (!empty($filter_params['uid'])) {
    $select->leftJoin('hamster_sources', 's', "t.$hsid = s.id");
    $select->condition('s.uid', $filter_params['uid']);
  }

  return $select->execute()->fetchAll(PDO::FETCH_ASSOC);
}






