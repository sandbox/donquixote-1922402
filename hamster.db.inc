<?php
/**
 * One day this will become a separate module ...
 * (or rather use dbtng instead?)
 */


/**
 * Similar to drupal_write_record, but can set fields to NULL.
 */
function _hamster_write_record($table, &$values, $update = NULL, $set_null = NULL) {

  $tbl = _hamster_sql_table($table);

  // Convert to an array if needed.
  $is_array = is_array($values);
  if (!$is_array) {
    $values = (array)$values;
  }

  $success = $tbl->writeRow($values, $update, $set_null);

  if (!$is_array) {
    $values = (object)$values;
  }
}


function _hamster_sql_table($table) {
  static $_cache = array();
  if (!isset($_cache[$table])) {
    $schema = drupal_get_schema($table);
    $_cache[$table] = $schema ? new _hamster_SqlTable($table, $schema) : FALSE;
  }
  return is_object($_cache[$table]) ? $_cache[$table] : NULL;
}


class _hamster_SqlTable {

  protected $_table;
  protected $_tableSql;
  protected $_schema;
  protected $_placeholders = array();
  protected $_defaults = array();
  protected $_serials = array();
  protected $_serialize = array();
  protected $_primary;

  function __construct($table, $schema) {
    $this->_table = $table;
    $this->_tableSql = '{' . $table . '}';
    $this->_schema = $schema;
    $this->_primary = array_combine($schema['primary key'], $schema['primary key']);
    foreach ($schema['fields'] as $k => $info) {
      $p = db_type_placeholder($info['type']);
      $this->_placeholders[$k] = $p ? $p : "'%s'";
      if (isset($info['default'])) {
        $this->_defaults[$k] = $info['default'];
      }
      if ($info['type'] === 'serial') {
        $this->_serials[$k] = $k;
      }
      if (!empty($info['serialize'])) {
        $this->_serialize[$k] = $k;
      }
    }
  }

  function writeRow(array &$values, $update = NULL, $set_null = NULL) {
    if ($update === FALSE) {
      return $this->insertRow($values);
    }
    else if ($update === TRUE) {
      return $this->updateRow($values, $set_null);
    }
    else if (is_string($update)) {
      // use the $update for the keys.
      $this->updateRow($values, array($update), $set_null);
    }
    else if (is_array($update)) {
      // use the $update for the keys.
      $this->updateRow($values, $update);
    }
    else {
      // now we have to find out ourselves if the row already exists.
      $cond = $this->_buildCond($values);
      if ($this->_rowExists($cond)) {
        return $this->_updateRow($values, $cond, $set_null);
      }
      else {
        return $this->insertRow($values);
      }
    }
  }

  function writeRows(array &$rows, array $fixed = NULL) {
    $cond = $this->_buildRowsCond($rows, $fixed);
    $existing = $this->_rowsExist($cond);
  }

  protected function _writeRowsCond(array $rows, array $fixed = NULL) {
    $cond = _hamster_sql_cond();
    $keys = $this->_primary;
    foreach ($fixed as $k => $v) {
      unset($keys[$k]);
      $placeholder = $this->_getPlaceholder($k);
      $cond->where("$k = $placeholder", $v);
    }
    $cond->whereInMulti($keys, $rows, $this->_placeholders);
    return $cond;
  }

  /**
   * Use this if you are sure the row exists.
   */
  function updateRow(array $values, array $keys = NULL, $set_null = NULL) {
    $cond = $this->_buildCond($values, $keys);
    if (is_array($set_null)) {
      foreach ($values as $k => $v) {
        if ($v === NULL) {
          unset($values[$k]);
        }
      }
      foreach ($set_null as $k => $null) {
        $values[$k] = NULL;
      }
    }
    $this->_updateRow($values, $cond);
  }

  function _updateRow(array $values, $cond) {
    $set = $this->_buildSet($values, $keys);
    $table_sql = $this->_tableSql;
    if ('' . $cond) {
      // now we know that $cond is not empty.
      $sql = "UPDATE $table_sql SET $set WHERE $cond";
      $args = array_merge($set->args(), $cond->args());
      db_query($sql, $args);
    }
  }

  /**
   * Use this if you are sure the row does not exists yet.
   */
  function insertRow(array &$values) {
    $values += $this->_defaults;
    $table_sql = $this->_tableSql;
    $set = $this->_buildSet($values);
    $sql = "INSERT INTO $table_sql SET $set";
    $args = $set->args();
    $success = db_query($sql, $args);
    if ($success) {
      // Get last insert ids and fill them in.
      foreach ($this->_serials as $k) {
        $values[$k] = db_last_insert_id($this->_table, $k);
      }
    }
  }

  protected function _buildCond(array $values, array $keys = NULL) {
    if (!is_array($keys)) {
      $keys = $this->_primary;
    }
    $cond = _hamster_sql_where();
    foreach ($keys as $k) {
      if (isset($values[$k])) {
        $placeholder = $this->_getPlaceholder($k);
        $v = $values[$k];
        if (!empty($this->_serialize[$k])) {
          $v = serialize($k);
        }
        $cond->where("$k = $placeholder", $v);
      }
      else {
        $cond->where("$k IS NULL");
      }
    }
    return $cond;
  }

  protected function _buildSet(array $values, array $keys = NULL) {
    if (!is_array($keys)) {
      $keys = $this->_primary;
    }
    foreach ($this->_serials as $k) {
      unset($values[$k]);
    }
    foreach ($keys as $k) {
      unset($values[$k]);
    }
    $set = _hamster_sql_set();
    foreach ($values as $k => $v) {
      if (!isset($this->_schema['fields'][$k])) {
        continue;
      }
      if ($this->_serialize[$k]) {
        $v = serialize($v);
      }
      $placeholder = $this->_getPlaceholder($k);
      $set->set($k, $v, $placeholder);
    }
    return $set;
  }

  protected function _getPlaceholder($key) {
    $p = $this->_placeholders[$key];
    return $p ? $p : "'%s'";
  }

  protected function _rowExists($cond) {
    $table_sql = $this->_tableSql;
    $fields = implode(', ', $this->_primary);
    $q = db_query("SELECT $fields FROM $table_sql WHERE $cond", $cond->args());
    return db_fetch_object($q) ? $cond : FALSE;
  }

  protected function _rowsExist($cond) {
    $table_sql = $this->_tableSql;
    $fields = implode(', ', $this->_primary);
    $q = db_query("SELECT $fields FROM $table_sql WHERE $cond", $cond->args());
    $rows = array();
    while ($row = db_fetch_object($q)) {
      $rows[] = $row;
    }
    return $rows;
  }
}


function _hamster_sql_set() {
  return new _hamster_SqlSet();
}


class _hamster_SqlSet {

  protected $_fragments = array();
  protected $_args = array();

  function set($string, $v, $placeholder = NULL) {
    if (!isset($placeholder)) {
      $this->_fragments[] = "\n  $string";
      $this->_args[] = $v;
    }
    else if (is_null($v)) {
      $this->_fragments[] = "\n  $string = NULL";
    }
    else {
      $this->_fragments[] = "\n  $string = $placeholder";
      $this->_args[] = $v;
    }
  }

  function __toString() {
    return implode(', ', $this->_fragments);
  }

  function args() {
    return $this->_args;
  }
}


/**
 * Taken from another module (menupoly)
 */
function _hamster_sql_where($string = NULL, $args = array()) {
  $cond = new _hamster_SqlWhere();
  if (isset($string)) {
    $cond->where($string, $args);
  }
  return $cond;
}


class _hamster_SqlWhere {

  protected $_where_fragments = array();
  protected $_where_args = array();

  function whereCond($cond) {
    $this->where('' . $cond, $cond->args());
  }

  function whereCondOr($conditions, $cond2 = NULL, $cond3 = NULL) {
    if (!is_array($conditions)) {
      $conditions = func_get_args();
    }
    $fragments = array();
    $args = array();
    foreach ($conditions as $cond) {
      $fragments[] = "($cond)";
      $args = array_merge($args, $cond->args());
    }
    $sql = implode(' OR ', $fragments);
    $this->where("($sql)", $args);
  }

  function where($string, $args = array()) {
    if ($string) {
      $this->_where_fragments[] = $string;
      if (is_array($args)) {
        foreach ($args as $arg) {
          $this->_where_args[] = $arg;
        }
      }
      else if (isset($args)) {
        $this->_where_args[] = $args;
      }
    }
    return $this;
  }

  function whereEquals($k, $v, $placeholder) {
    if (is_null($v)) {
      $this->where("$k IS NULL");
    }
    else {
      $this->where("$k = $placeholder", $v);
    }
    return $this;
  }

  function whereFields(array $values, $table_alias = NULL, array $placeholders = array()) {
    $prefix = $table_alias ? ($table_alias . '.') : '';
    foreach ($values as $k => $v) {
      // TODO: Is this placeholder guessing really safe?
      $placeholder = isset($placeholders[$k]) ? $placeholders[$k] : $this->_guessPlaceholder($v);
      $this->where($prefix . "$k = $placeholder", $v);
    }
    return $this;
  }

  function whereContains($string, $arg) {
    $like = ($GLOBALS["db_type"] == 'pgsql') ? "ILIKE" : "LIKE";
    $this->where("$string $like '%%%s%%'", $arg);
    return $this;
  }

  function whereBegins($string, $arg) {
    $like = ($GLOBALS["db_type"] == 'pgsql') ? "ILIKE" : "LIKE";
    $this->where("$string $like '%s%%'", $arg);
    return $this;
  }

  function whereWordBegins($string, $arg) {
    $like = ($GLOBALS["db_type"] == 'pgsql') ? "ILIKE" : "LIKE";
    $this->where("($string $like '%s%%' OR $string $like '%% %s%%')", array($arg, $arg));
    return $this;
  }

  function whereLike($string, $arg) {
    $like = ($GLOBALS["db_type"] == 'pgsql') ? "ILIKE" : "LIKE";
    $this->where("$string $like '%s'", $arg);
    return $this;
  }

  function whereIn($string, $args, $placeholder = "'%s'") {
    if (!count($args)) {
      $this->where("false");
    }
    else {
      $placeholders = implode(', ', array_fill(0, count($args), $placeholder));
      $this->where("$string IN ($placeholders)", $args);
    }
    return $this;
  }

  function whereNotIn($string, $args, $placeholder = "'%s'") {
    $placeholders = implode(', ', array_fill(0, count($args), $placeholder));
    $this->where("$string NOT IN ($placeholders)", $args);
    return $this;
  }

  function whereInMulti(array $keys, array $rows, array $placeholders) {
    if (count($keys) > 1) {
      _whereInMulti($distinct, $rows, $placeholders);
    }
    else if (count($keys) === 1) {
      foreach ($keys as $k) {}
      $placeholder = $placeholders[$k];
      $args = array();
      foreach ($rows as $row) {
        $args[] = $row[$k];
      }
      $this->whereIn($k, $args, $placeholder);
    }
    return $this;
  }

  protected function _whereInMulti(array $keys, array $rows, array $placeholders) {
    $distinct = $this->_sortCountDistinct($keys, $rows);
    $keys = array_keys($distinct);
    $k = array_shift($keys);
    $rows_sorted = array();
    foreach ($rows as $row) {
      $v = $row[$k];
      $rows_sorted[$v][] = $row;
    }
    $fragments = array();
    $args = array();
    foreach ($distinct[$k] as $v) {
      if (!isset($rows_sorted[$v])) {
        continue;
      }
      $cond = _hamster_sql_where();
      $cond->whereEquals($k, $v, $placeholders[$k]);
      $cond->whereInMulti($keys, $rows_sorted[$v], $placeholders);
      $fragments[] = $cond->__toString();
      $args = array_merge($args, $cond->args());
    }
    $sql = implode(' OR ', $fragments);
    $this->where("($sql)", $args);
  }

  protected function _sortCountDistinct($keys, $rows) {
    $distinct = array();
    $count = array();
    foreach ($keys as $k) {
      foreach ($rows as $row) {
        $v = $row[$k];
        $distinct[$k][$v] = $v;
      }
      $count[$k] = count($distinct[$k]);
    }
    array_multisort($count, $distinct);
    return $distinct;
  }

  function splitWhere($string, $args = array()) {
    $new = $this->cloneWhere($string, $args);
    $this->where("NOT ($string)", $args);
    return $new;
  }

  function isEmpty() {
    return !count($this->_where_fragments);
  }

  function getWhereString($cond = NULL) {
    $where_fragments = $this->_where_fragments;
    if ($cond) {
      $where_fragments[] = "($cond)";
    }
    if (count($where_fragments)) {
      return implode("  AND\n        ", $where_fragments);
    }
    else {
      // just return TRUE
      return '1';
    }
  }

  function getWhereArgs($args = NULL) {
    $where_args = $this->_where_args;
    if (is_array($args)) {
      $where_args += $args;
    }
    return $where_args;
  }

  function args() {
    return $this->getWhereArgs();
  }

  function __toString() {
    return $this->getWhereString();
  }

  function cloneWhere($string, $args = array()) {
    return $this->_clone()->where($string, $args);
  }

  protected function _clone() {
    $new = new self;
    $new->_where_args = $this->_where_args;
    $new->_where_fragments = $this->_where_fragments;
    return $new;
  }

  protected function _guessPlaceholder($val) {
    if (is_numeric($val)) {
      return "%d";
    }
    else {
      return "'%s'";
    }
  }
}




