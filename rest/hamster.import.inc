<?php

/**
 * Demo page to test the API.
 */
function hamster_import_page() {

  $challenge = hamster('challenge')->generatePersonal();
  $challenge_response = "I am logged in, no response needed.";

  $source_options[NULL] = '-- Please choose --';
  $q = db_select('hamster_sources', 's');
  $q->fields('s', array('id', 'name'));
  $q->condition('uid', $GLOBALS['user']->uid);
  $source_options += $q->execute()->fetchAllKeyed();

  $w = 0;
  $form = array(
    '#type' => 'form',
    '#action' => url('hamster/import/post'),
    '#attributes' => array('enctype' => 'multipart/form-data'),
    'source_id' => array(
      '#weight' => ++$w,
      '#type' => 'select',
      '#title' => 'Source',
      '#options' => $source_options,
    ),
    'upload' => array(
      '#weight' => ++$w,
      '#type' => 'file',
      '#name' => 'upload',
      '#title' => 'File to import',
      '#description' => 'Upload your sqlite file, typically named "hamster.db".',
    ),
    'challenge' => array(
      '#weight' => ++$w,
      '#type' => 'textfield',
      '#title' => 'Challenge',
      '#value' => $challenge,
      '#attributes' => array('readonly' => TRUE),
    ),
    'challenge_response' => array(
      '#weight' => ++$w,
      '#type' => 'textfield',
      '#title' => 'Challenge response',
      '#value' => $challenge_response,
    ),
    'submit' => array(
      '#weight' => ++$w,
      '#type' => 'submit',
      '#value' => 'Import',
    ),
  );

  $form_state = array();
  $form = form_builder(NULL, $form, $form_state);
  $html = drupal_render($form);

  return $html;
}

/**
 * RESTful import API.
 * This is a "page callback".
 */
function hamster_import_post() {
  $response = _hamster_import_post();
  if (is_array($response)) {
    drupal_json($response);
    return;
  }
  if (empty($GLOBALS['user']->uid)) {
    drupal_json(array('response' => $response));
    return;
  }
  return $response;
}


function _hamster_import_post() {

  dpm($_POST);
  dpm($_FILES);

  if (0
    || empty($_FILES['upload']['tmp_name'])
    || !is_file($_FILES['upload']['tmp_name'])
    || empty($_POST['source_id'])
    || (1
      && !is_string($_POST['source_id'])
      && !is_numeric($_POST['source_id'])
    )
    || empty($_POST['challenge'])
    || empty($_POST['challenge_response'])
  ) {
    $challenge = hamster('challenge')->generate();
    return array('challenge' => $challenge);
  }

  $source = hamster('loader')->hamster_source($_POST['source_id']);

  if (!is_object($source)) {
    return "No source with name or id = " . check_plain($_POST['source_id']);
  }

  $file_info = $_FILES['upload'];
  $file = $file_info['tmp_name'];

  $allowed = hamster('challenge')->checkFileImport($_POST['challenge'], $source->name, $file, $_POST['challenge_response']);

  if (!$allowed) {
    dpm('Challenge response mismatch.');
    return 'Challenge response mismatch';
  }

  // Import from the sqlite.
  $importProcess = new hamster_Import_ImportProcess($source->id);
  $importProcess->importFile($file);

  return 'success';
}
