<?php


function hamster_categories_form(array $form_state, array $filter_params = array()) {

  $form = array();

  $form['items'] = array();

  $form['multicrud'] = _hamster_categories_multicrud_element($filter_params);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
    '#attributes' => array('tabindex' => 1),
  );

  $form['#filter_params'] = $filter_params;

  // generic submit handler provided by multicrud core.
  $form['#submit'] = array('multicrud_submit_all');

  return $form;
}


function _hamster_categories_multicrud_element(array $filter_params) {

  $element = array(
    '#type' => 'multicrud',

    // form elements for each row
    '#multicrud_elements' => _hamster_categories_multicrud_row_elements($filter_params),

    // value handler to load and save
    '#multicrud_valueHandler' => new hamster_MulticrudValueHandler_Categories($filter_params),

    // disable tabledrag and hierarchical behavior.
    '#multicrud_weights' => FALSE,
    '#multicrud_parents' => FALSE,
    '#multicrud_insert' => FALSE,
    '#multicrud_delete' => FALSE,
  );

  return $element;
}


function _hamster_categories_multicrud_row_elements(array $filter_params) {

  $elements = array();
  $elements['category'] = array(
    '#title' => 'Category',
    '#input' => TRUE,
  );
  $elements['source'] = array(
    '#title' => 'Source',
    '#input' => TRUE,
  );
  $elements['duration'] = array(
    '#title' => 'Duration',
    '#input' => TRUE,
  );
  $elements['earliest'] = array(
    '#title' => 'Earliest',
    '#input' => TRUE,
  );
  $elements['latest'] = array(
    '#title' => 'Latest',
    '#input' => TRUE,
  );
  /*
  $elements['count'] = array(
    '#title' => 'Count',
    '#input' => TRUE,
  );
  */
  $elements['n_activities'] = array(
    '#title' => '#A',
    '#input' => TRUE,
  );
  $elements['n_facts'] = array(
    '#title' => '#F',
    '#input' => TRUE,
  );
  $elements['associated_nid'] = array(
    '#title' => 'Drupal project',
    '#type' => 'textfield',
    '#size' => 22,
    '#autocomplete_path' => 'hamster/autocomplete/category',
  );

  if (!empty($filter_params['hamster_source_id'])) {
    unset($elements['source']);
  }

  return $elements;
}
