<?php


function hamster_sources_form(array $form_state, array $filter_params = array()) {

  $form = array();

  $form['multicrud'] = _hamster_sources_multicrud_element($filter_params);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
    '#attributes' => array('tabindex' => 1),
  );

  $form['#filter_params'] = $filter_params;

  // generic submit handler provided by multicrud core.
  $form['#submit'] = array('multicrud_submit_all');

  return $form;
}


function _hamster_sources_multicrud_element(array $filter_params) {

  $element = array(
    '#type' => 'multicrud',

    // form elements for each row
    '#multicrud_elements' => _hamster_sources_multicrud_row_elements($filter_params),

    // value handler to load and save
    '#multicrud_valueHandler' => _hamster_sources_multicrud_value_handler($filter_params),

    // disable tabledrag and hierarchical behavior.
    '#multicrud_weights' => FALSE,
    '#multicrud_parents' => FALSE,
  );

  return $element;
}


function _hamster_sources_multicrud_row_elements(array $filter_params) {

  $elements = array();
  $elements['link'] = array(
    '#title' => 'Link',
    '#input' => TRUE,
  );
  $elements['name'] = array(
    '#title' => 'Name',
    '#type' => 'textfield',
    '#size' => 12,
  );
  $elements['uid'] = array(
    '#title' => 'uid',
    '#type' => 'textfield',
    '#size' => 4,
  );
  $elements['import_dir'] = array(
    '#title' => 'Import directory',
    '#type' => 'textfield',
    '#size' => 24,
  );
  $elements['description'] = array(
    '#title' => 'Description',
    '#type' => 'textarea',
    '#cols' => 8,
    '#rows' => 1,
    '#resizable' => FALSE,
  );

  if (isset($filter_params['uid'])) {
    unset($elements['uid']);
  }

  return $elements;
}


/**
 * Factory function for the value handler
 */
function _hamster_sources_multicrud_value_handler($filter_params) {
  return new _hamster_sources_MulticrudValueHandler($filter_params);
}


/**
 * The value handler is responsible for loading, saving and validation of items.
 */
class _hamster_sources_MulticrudValueHandler extends multicrud_ValueHandler {

  protected $_filterParams;
  protected $_originalItems;
  protected $_linkOptions;
  protected $_saveErrors = array();

  /**
   * @param $type
   *   the node type
   * @param $langSpecificFields
   *   names of fields that are not shared across languages
   */
  function __construct(array $filter_params) {
    $this->_filterParams = $filter_params;
    $this->_originalItems = _hamster_sources_multicrud_default_value($filter_params);
    $this->_linkOptions = array();
    foreach (array('date_begin', 'date_end') as $key) {
      if (!empty($filter_params[$key])) {
        $this->_linkOptions['query'][$key] = $filter_params[$key];
      }
    }
    module_load_include('inc', 'hamster', 'hamster.db');
  }

  function validateItem($item, $meta, $error_handler) {
    // TODO: Implement item validation
  }

  function getDefaultValue() {
    $items = $this->_originalItems;
    foreach ($items as &$item) {
      $link_path = 'hamster/' . $item['id'];
      $item['link'] = l((int)$item['n_categories'] . ' categories', $link_path, $this->_linkOptions);
    }
    return $items;
  }

  /**
   * Called from multicrud for to-be-deleted items.
   */
  function deleteItem($id) {
    if (!isset($this->_originalItems[$id])) {
      // no item to delete.
      return;
    }
    foreach (array('categories', 'activities', 'facts', 'tags') as $type) {
      $tbl = '{hamster_' . $type . '}';
      $q = db_query("SELECT id FROM $tbl WHERE hamster_source_id = %d", $id);
      if (db_fetch_object($q)) {
        // deleting would leave an orphan value.
        return FALSE;
      }
    }
    db_query("DELETE FROM hamster_sources WHERE id = %d", $id);
    return TRUE;
  }

  /**
   * Called from multicrud for to-be-updated items.
   */
  function updateItem($id, $item, $meta) {
    if (!$this->_validateItem($item)) {
      return FALSE;
    }
    if (!isset($this->_originalItems[$id])) {
      // we cannot update something that does not exist.
      return;
    }
    $item['id'] = $id;
    if (!empty($this->_filterParams['uid'])) {
      $item['uid'] = $this->_filterParams['uid'];
    }
    $success = _hamster_write_record('hamster_sources', $item, array('id'));
    return $success;
  }

  /**
   * Called from multicrud for to-be-created items.
   */
  function insertItem($item, $meta) {
    if (!$this->_validateItem($item)) {
      return FALSE;
    }
    if (!empty($this->_filterParams['uid'])) {
      $item['uid'] = $this->_filterParams['uid'];
    }
    $success = _hamster_write_record('hamster_sources', $item);
    return $item['id'];
  }

  protected function _validateItem($item) {
    return TRUE;
  }
}


function _hamster_sources_multicrud_default_value(array $filter_params) {

  $select_sum = db_select('hamster_categories', 'c');
  $select_sum->addField('c', 'hamster_source_id');
  $select_sum->addExpression('COUNT(*)', 'n_categories');
  $select_sum->groupBy('c.hamster_source_id');

  $select = db_select('hamster_sources', 's');
  $select->fields('s');
  $select->leftJoin($select_sum, 'sum', 'sum.hamster_source_id = s.id');
  $select->fields('sum', array('n_categories'));
  if (!empty($filter_params['uid'])) {
    $select->condition('s.uid', $filter_params['uid']);
  }
  $sources = $select->execute()->fetchAll(PDO::FETCH_ASSOC);
  $result = array();
  foreach ($sources as $source) {
    $result[$source['id']] = $source;
  }
  return $result;
}






