<?php

/**
 * Implements hook_drush_command().
 */
function hamster_drush_command() {
  $items['hamster-sources'] = array(
    'description' => 'Show a list of hamster sources.',
  );
  $items['hamster-add-source'] = array(
    'description' => 'Add another hamster source.',
    'arguments' => array(
      'name' => 'Name of the new hamster source',
      'user' => 'Username or uid of the user to associate with this source.',
      'description' => 'Optional. Description of this source.'
    ),
  );
  $items['hamster-projects-autosync'] = array(
    'description' => 'Automatically associate hamster categories with Drupal projects based on og_mailinglist keys.',
    'arguments' => array(
      'sources' => 'Name or id of the hamster source. If not specified, all sources are synced.',
    ),
  );
  return $items;
}

/**
 * Drush command to show a list of hamster sources
 */
function drush_hamster_sources() {

  $q = hamster('queryBuilder')->select('sources');
  $q->addField('s', 'api_key');
  $order =& $q->getOrderBy();
  $order = array();
  $q->orderBy('s.name', 'ASC');
  $q->orderBy('latest', 'ASC');

  $rows = $q->execute()->fetchAll(PDO::FETCH_OBJ);

  $tbl[] = array('User', 'Source', 'API key', 'Hours', '#Objects', 'Description');
  foreach ($rows as $row) {
    $tbl[$row->name] = array(
      $row->uid . ': ' . $row->username,
      $row->id . ': ' . $row->name,
      $row->api_key,
      number_format($row->duration / (60 * 60)),
      (int)$row->n_categories . ', ' . (int)$row->n_activities . ', ' . (int)$row->n_facts,
      $row->description,
    );
  }

  drush_print_table($tbl);
}

/**
 * Drush command to add a hamster source
 */
function drush_hamster_add_source($name, $user, $description = NULL) {

  // find the user
  $q = db_select('users', 'u');
  $q->fields('u', array('uid', 'name'));
  if (is_numeric($user) && ('' . (int)$user) === ('' . $user) && $user > 0) {
    $q->condition('u.uid', $user);
  }
  else {
    $q->condition('u.name', $user);
  }
  $u = $q->execute()->fetchObject();

  if (!$u) {
    drush_log("No user found for '$user'", 'error');
    return;
  }

  // check for duplicates
  $q = db_select('hamster_sources', 's');
  $q->addField('s', 'id');
  $q->condition('s.name', $name);
  $s = $q->execute()->fetchObject();
  if ($s) {
    drush_log("A source with the name '$name' already exists. Please choose a different name.", 'error');
    return;
  }

  if (isset($import_dir) && !is_dir($import_dir)) {
    drush_log("The given import dir '$import_dir' does not exist, or is not a directory.", 'error');
  }

  $values = array(
    'name' => $name,
    'uid' => $u->uid,
    'api_key' => hamster_Util::randomString(),
    'description' => $description,
  );
  $success = drupal_write_record('hamster_sources', $values);

  if ($success) {
    drush_log("Created a hamster source with name '$name'", 'success');
  }
}

/**
 * Drush command to associate Hamster categories with Drupal projects based on og_mailinglist key.
 */
function drush_hamster_projects_autosync($source_id = NULL) {

  $q = db_select('hamster_categories', 'c');
  $q->fields('c', array('hamster_source_id', 'id', 'name'));
  $q->isNull('c.associated_nid');

  if (!empty($source_id)) {
    $source = hamster_source_load($source_id);
    if (empty($source)) {
      drush_log("No source was found for '$source_id'.");
    }
    $q->condition('o.hamster_source_id', $source->id);
  }

  $q->leftJoin('hamster_sources', 's', 's.id = c.hamster_source_id');
  $q->fields('s', array('uid'));
  $q->addField('s', 'name', 'source_name');

  $c_by_name = array();
  foreach ($q->execute() as $row) {
    $c_by_name[strtolower(trim($row->name))][$row->hamster_source_id][] = $row->id;
  }

  $q = db_select('og_mailinglist', 'o');
  $q->fields('o', array('nid', 'group_email'));
  $o_by_name = array();
  foreach ($q->execute() as $row) {
    $o_by_name[strtolower(trim($row->group_email))][] = $row->nid;
  }

  ksort($c_by_name);
  ksort($o_by_name);

  $not_found = array();
  foreach ($c_by_name as $name => $categories) {
    if (empty($o_by_name[$name])) {
      $not_found[] = $name;
    }
    elseif (count($o_by_name[$name]) > 1) {
      drush_log("More than one project node for '$name'.", 'warning');
    }
    else {
      $nid = $o_by_name[$name][0];
      foreach ($categories as $source_id => $cat_ids) {
        $q = db_update('hamster_categories');
        $q->condition('hamster_source_id', $source_id);
        $q->condition('id', $cat_ids);
        $q->fields(array('associated_nid' => $nid));
        $q->execute();
        $str = implode('|', $cat_ids);
        drush_log("Associate projects named $name ($str):$source_id with node $nid.", 'ok');
      }
    }
  }

  if (!empty($not_found)) {
    $not_found = implode("', '", $not_found);
    drush_log("No project nodes found for '$not_found'.", 'warning');
  }

  $categories = hamster('collections')->fetchAll('categories', $params);
}


