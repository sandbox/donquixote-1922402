<?php


function hamster_admin_form() {

  $form = array();

  $options = array();
  foreach (node_get_types() as $typename => $typeinfo) {
    $options[$typename] = $typeinfo->name;
  }

  foreach (array(
    'category' => 'Category',
    'activity' => 'Activity',
    'tag' => 'Tag',
    'fact' => 'Fact',
  ) as $type => $title) {
    $type_settings = variable_get('hamster_settings__' . $type, array());
    $form['hamster_settings__' . $type] = array(
      '#title' => 'Hamster ' . $title,
      '#type' => 'fieldset',
      '#tree' => TRUE,
      'node_types' => array(
        '#title' => 'Referenceable node types',
        '#type' => 'select',
        '#options' => $options,
        '#multiple' => TRUE,
        '#default_value' => $type_settings['node_types'],
      ),
    );
  }

  return system_settings_form($form);
}




