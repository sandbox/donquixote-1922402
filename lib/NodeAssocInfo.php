<?php

/**
 * An object that knows which hamster type can be associated with which node
 * type.
 */
class hamster_NodeAssocInfo {

  protected $byHamsterType = array();
  protected $byNodeType = array();

  function __construct($info) {
    $this->byHamsterType = $info;
    foreach ($info as $hamster_type => $hamster_type_info) {
      if (!empty($hamster_type_info['node_types'])) {
        foreach ($hamster_type_info['node_types'] as $node_type) {
          $this->byNodeType[$node_type][$hamster_type] = $hamster_type;
          $this->byHamsterType[$hamster_type][$node_type] = $node_type;
        }
      }
    }
  }

  function nodeTypeToHamsterType($node_type) {
    // Just pick the first type that can be associated with the node.
    return reset($this->nodeTypeToHamsterTypes($node_type));
  }

  function nodeTypeToHamsterTypes($node_type) {
    return isset($this->byNodeType[$node_type]) ? $this->byNodeType[$node_type] : array();
  }

  function nodeTypeSupported($node_type) {
    return !empty($this->byNodeType[$node_type]);
  }
}
