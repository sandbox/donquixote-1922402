<?php

class hamster_Db_CollectionGateway {

  protected $queryBuilder;
  protected $collections = array();
  protected $periodic = array();
  protected $periodRanges = array();

  function __construct($query_builder) {
    $this->queryBuilder = $query_builder;
  }

  function fetchOne($list_type, $params) {
    return $this->get($list_type)->fetchOne($params);
  }

  function fetchAll($list_type, $params) {
    return $this->get($list_type)->fetchAll($params);
  }

  function fetchAllPeriodic($period_type, $list_type, $params) {
    return $this->getPeriodic($period_type, $list_type)->fetchAll($params);
  }

  function get($list_type) {
    if (!isset($this->collections[$list_type])) {
      $this->collections[$list_type] = new hamster_Db_Collection_OfObjects($this->queryBuilder, $list_type);
    }
    return $this->collections[$list_type];
  }

  function getPeriodic($period_type, $list_type) {
    if (!isset($this->periodic[$period_type][$list_type])) {
      $period_range = $this->periodRange($period_type);
      $this->periodic[$period_type][$list_type] = new hamster_Db_Collection_Periodic($this->queryBuilder, $list_type, $period_range);
    }
    return $this->periodic[$period_type][$list_type];
  }

  function periodRange($period_type) {
    if (!isset($this->periodRanges[$period_type])) {
      $this->periodRanges[$period_type] = $this->buildPeriodRange($period_type);
    }
    return $this->periodRanges[$period_type];
  }

  protected function buildPeriodRange($period_type) {
    switch ($period_type) {
      case 'month':
      case 'months':
        return new hamster_PeriodRange_Months();
    }
  }
}
