<?php

class hamster_Db_QueryBuilder_MetaJoiner {

  protected $q;
  protected $conditionBuilder;

  function __construct($condition_builder) {
    $this->conditionBuilder = $condition_builder;
  }

  function joinMeta($q, $type) {
    $this->q = $q;
    $this->step($type);
  }

  protected function step($type) {
    $this->conditionBuilder->cond($this->q, $type);
    $this->$type();
  }

  protected function facts() {
    $this->q->addField('f', 'description');
    $this->q->leftJoin('hamster_activities', 'a', 'a.id = f.activity_id AND a.deleted != 1 AND a.hamster_source_id = f.hamster_source_id');
    $this->step('activities');
  }

  protected function activities() {
    $this->q->addField('a', 'name', 'activity_name');
    $this->q->addField('a', 'id', 'activity_id');
    $this->q->addField('a', 'associated_nid', 'activity_nid');
    $this->q->leftJoin('hamster_categories', 'c', 'c.id = a.category_id AND c.deleted != 1 AND c.hamster_source_id = a.hamster_source_id');
    $this->step('categories');
  }

  protected function categories() {
    $this->q->addField('c', 'name', 'category_name');
    $this->q->addField('c', 'id', 'category_id');
    $this->q->addField('c', 'associated_nid', 'category_nid');
    $this->q->leftJoin('hamster_sources', 's', 's.id = c.hamster_source_id');
    $this->q->leftJoin('node', 'c_node', 'c_node.nid = c.associated_nid');
    $this->q->addField('c_node', 'title', 'category_node_title');
    $this->step('sources');
  }

  protected function sources() {
    $this->q->addField('s', 'name', 'source_name');
    $this->q->addField('s', 'uid');
    $this->q->leftJoin('users', 'u', "u.uid = s.uid");
    $this->q->addField('u', 'name', 'username');
  }
}
