<?php

class hamster_Db_QueryBuilder_AggregateBuilder {

  protected $conditionBuilder;
  protected $joinType = 'LEFT OUTER';

  function __construct($condition_builder) {
    $this->conditionBuilder = $condition_builder;
  }

  function select($type) {
    if (isset($type)) {
      // Aggregate grouped and keyed by $type id.
      return $this->$type();
    }
    else {
      // Non-grouped aggregate.
      $q = db_select($this->get('sources'), 'agg');
      $this->aggregateFields($q, 's');
      $q->addExpression('COUNT(DISTINCT agg.id)', 'n_sources');
      return $q;
    }
  }

  function joinAgg($q, $alias, $q_agg) {
    $cond = $this->conditionBuilder->joinAggCondition($alias);
    $q->addJoin($this->joinType, $q_agg, 'agg', $cond);
  }

  protected function get($type) {
    $q = $this->$type();
    return $q;
  }

  protected function facts() {
    $q = db_select('hamster_facts', 'f');
    $this->cond($q, 'facts');
    $expr = $this->conditionBuilder->intervalExpr();
    $q->addExpression($expr['duration'], 'duration');
    $q->addExpression($expr['earliest'], 'earliest');
    $q->addExpression($expr['latest'], 'latest');
    $q->addField('f', 'id');
    return $q;
  }

  protected function activities() {
    // TODO: Filter!
    $q = $this->build('facts', 'f');
    $expr = $this->conditionBuilder->intervalExpr();
    $q->addExpression("SUM($expr[duration])", 'duration');
    $q->addExpression("MIN($expr[earliest])", 'earliest');
    $q->addExpression("MAX($expr[latest])", 'latest');
    $q->addField('f', 'activity_id', 'id');
    $q->addField('f', 'hamster_source_id');
    $q->groupBy('f.activity_id');
    $q->groupBy('f.hamster_source_id');
    return $q;
  }

  protected function categories() {
    // TODO: Filter!
    $q = $this->build('activities', 'a');
    $this->joinAgg($q, 'a', $this->get('activities'));
    $this->aggregateFields($q, 'a');
    $q->addField('a', 'category_id', 'id');
    $q->addField('a', 'hamster_source_id');
    $q->groupBy('a.category_id');
    $q->groupBy('a.hamster_source_id');
    return $q;
  }

  protected function sources() {
    // TODO: Filter!
    $q = $this->build('categories', 'c');
    $this->joinAgg($q, 'c', $this->get('categories'));
    $this->aggregateFields($q, 'c');
    $q->addField('c', 'hamster_source_id', 'id');
    $q->groupBy('c.hamster_source_id');
    return $q;
  }

  protected function build($type, $alias) {
    $q = db_select('hamster_' . $type, $alias);
    $this->cond($q, $type);
    $q->addExpression("COUNT(DISTINCT $alias.id)", 'n_' . $type);
    return $q;
  }

  protected function cond($q, $type) {
    $filtered = $this->conditionBuilder->cond($q, $type);
    if ($filtered) {
      $this->joinType = 'INNER';
    }
  }

  protected function aggregateFields($q, $alias) {
    switch ($alias) {
      case 's':
        $q->addExpression('SUM(agg.n_categories)', 'n_categories');
      case 'c':
        $q->addExpression('SUM(agg.n_activities)', 'n_activities');
      case 'a':
        $q->addExpression('SUM(agg.n_facts)', 'n_facts');
      case 'f':
        $q->addExpression('SUM(agg.duration)', 'duration');
        $q->addExpression('MIN(agg.earliest)', 'earliest');
        $q->addExpression('MAX(agg.latest)', 'latest');
    }
  }
}
