<?php

class hamster_Db_QueryBuilder_ConditionBuilder {

  protected $params;
  protected $isFiltered;

  function __construct($params) {
    $this->params = $params;
  }

  function intervalExpr() {

    $interval_start_expr = 'f.start_time';
    $interval_end_expr = 'f.end_time';

    if (!empty($this->params['time_begin'])) {
      $time_begin = (int)$this->params['time_begin'];
      $interval_start_expr = "GREATEST(f.start_time, $time_begin)";
    }

    if (!empty($this->params['time_end'])) {
      $time_end = (int)$this->params['time_end'];
      $interval_end_expr = "LEAST(f.end_time, $time_end)";
    }

    return array(
      'earliest' => $interval_start_expr,
      'latest' => $interval_end_expr,
      'duration' => "$interval_end_expr - $interval_start_expr",
    );
  }

  function fieldAndCond($q, $alias, $field, $param_key = NULL) {
    if (!isset($param_key)) {
      $param_key = $field;
    }
    $q->addField($alias, $field, $param_key);
    if (!empty($this->params[$param_key])) {
      $q->where("$alias.$field = " . (int)$this->params[$param_key]);
    }
  }

  function joinAggCondition($alias) {
    $cond = array();
    $cond[] = "$alias.id = agg.id";
    if (empty($this->params['hamster_source_id'])) {
      if ($alias !== 's') {
        $cond[] = "$alias.hamster_source_id = agg.hamster_source_id";
      }
    }
    return !empty($cond) ? implode(' AND ', $cond) : NULL;
  }

  function joinCond($field, $alias) {
    $cond[] = $this->basicJoinCond($field);
    $cond[] = "($alias.deleted IS NULL OR $alias.deleted != 1)";
  }

  protected function basicJoinCond($field, $a, $b) {
    switch ($field) {
      case 'activity_id':
        return "f.activity_id = a.id";
      case 'category_id':
        return "a.category_id = c.id";
      case 'hamster_source_id':
    }
  }

  function cond($q, $type) {
    $this->isFiltered = FALSE;
    $alias = $this->getAlias($type);
    $this->$type($q);
    if ($alias !== 's') {
      $q->where("$alias.deleted IS NULL OR $alias.deleted != 1");
    }
    return $this->isFiltered;
  }

  function fact_tags($q) {
    if ($this->sourceCondition($q, 'ft')) {
      $this->paramCondition($q, 'ft', 'fact_id');
      $this->paramCondition($q, 'ft', 'tag_id');
    }
  }

  function facts($q) {
    if ($this->sourceCondition($q, 'f')) {
      $this->paramCondition($q, 'f', 'id', 'fact_id');
      $this->paramCondition($q, 'f', 'activity_id');
    }
    $this->paramCondition($q, 'f', 'associated_nid', 'fact_nid');

    if (!empty($this->params['time_begin'])) {
      $time_begin = (int)$this->params['time_begin'];
      $q->where('f.end_time > ' . $time_begin);
      $this->isFiltered = TRUE;
    }

    if (!empty($this->params['time_end'])) {
      $time_end = (int)$this->params['time_end'];
      $q->where('f.start_time < ' . $time_end);
      $this->isFiltered = TRUE;
    }
  }

  function activities($q) {
    if ($this->sourceCondition($q, 'a')) {
      $this->paramCondition($q, 'a', 'id', 'activity_id');
      $this->paramCondition($q, 'a', 'category_id');
    }
    $this->paramCondition($q, 'a', 'associated_nid', 'activity_nid');
  }

  function categories($q) {
    if ($this->sourceCondition($q, 'c')) {
      $this->paramCondition($q, 'c', 'id', 'category_id');
    }
    $this->paramCondition($q, 'c', 'associated_nid', 'category_nid');
  }

  function tags($q) {
    if ($this->sourceCondition($q, 't')) {
      $this->paramCondition($q, 't', 'id', 'tag_id');
    }
    $this->paramCondition($q, 't', 'associated_nid', 'tag_nid');
  }

  function sources($q) {
    $this->paramCondition($q, 's', 'id', 'hamster_source_id');
    $this->paramCondition($q, 's', 'uid');
  }

  protected function sourceCondition($q, $alias) {
    if (!empty($this->params['hamster_source_id'])) {
      $q->where("$alias.hamster_source_id = " . $this->params['hamster_source_id']);
      return TRUE;
    }
  }

  protected function paramCondition($q, $alias, $field, $param_key = NULL) {
    if (!isset($param_key)) {
      $param_key = $field;
    }
    if (!empty($this->params[$param_key])) {
      $q->where("$alias.$field = " . $this->params[$param_key]);
    }
  }

  protected function getAlias($type) {
    return ($type === 'fact_tags') ? 'ft' : $type{0};
  }
}
