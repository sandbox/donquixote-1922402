<?php

class hamster_Db_Collection_Periodic {

  protected $qb;
  protected $type;
  protected $periodRange;

  function __construct($query_builder, $type, $period_range) {
    $this->qb = $query_builder;
    $this->type = $type;
    $this->periodRange = $period_range;
  }

  function fetchAll($params) {
    // Determine periods interval.
    $ = $this->qb->select(NULL, $params);
    $rows = $q->execute()->fetchAll();
    $row = $rows[0];
    $all = array();
    foreach ($this->periodRange->periods($row->earliest, $row->latest) as $period) {
      $modified_params = $this->periodParams($params, $period);
      if (FALSE === $params) {
        continue;
      }
      $q = $this->qb->select($this->type, $modified_params);
      $some = $q->execute()->fetchAllAssoc('_primary');
      foreach ($some as $key => $one) {
        $one->period = $period;
        $one->period_begin = $period->time_begin;
        $primary = $period->slug;
        if (isset($this->type)) {
          $primary .= '.' . $key;
        }
        elseif (!empty($one->duration)) {
          if (!isset($min_index)) {
            $min_index = count($all);
          }
          $max_index = count($all);
        }
        $all[$primary] = $one;
      }
    }
    if (!isset($this->type)) {
      if (isset($min_index) && isset($max_index)) {
        $keys = array_slice(array_keys($all), $min_index, $max_index - $min_index + 1);
        $all_excerpt = array();
        foreach ($keys as $key) {
          $all_excerpt[$key] = $all[$key];
        }
        return $all_excerpt;
      }
      else {
        return array();
      }
    }
    else {
      return $all;
    }
  }

  protected function periodParams($params, $period) {
    if (isset($params['time_begin'])) {
      $params['time_begin'] = max($params['time_begin'], $period->time_begin);
    }
    else {
      $params['time_begin'] = $period->time_begin;
    }
    if (isset($params['time_end'])) {
      $params['time_end'] = min($params['time_end'], $period->time_end);
    }
    else {
      $params['time_end'] = $period->time_end;
    }
    if ($params['time_end'] <= $params['time_begin']) {
      $params = FALSE;
    }
    return $params;
  }
}
