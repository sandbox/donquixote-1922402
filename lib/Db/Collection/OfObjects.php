<?php

class hamster_Db_Collection_OfObjects {

  protected $qb;
  protected $type;

  function __construct($query_builder, $type) {
    $this->qb = $query_builder;
    $this->type = $type;
  }

  function fetchOne($params) {
    $all = $this->fetchAll($params);
    foreach ($all as $row) {
      return $row;
    }
  }

  function fetchAll($params = array()) {
    $q = $this->qb->select($this->type, $params);
    $tables =& $q->getTables();
    try {
      $all = $q->execute()->fetchAllAssoc('_primary');
    }
    catch (Exception $e) {
      dpm('DB ERROR');
      dpm($e);
      hamster_dpq($q);
    }
    $method = 'enhanceRows_' . $this->type;
    if (method_exists($this, $method)) {
      $this->$method($all);
    }
    return $all;
  }

  function enhanceRows_facts(&$all) {

    $keys = array();
    foreach ($all as &$row) {
      $row->tags = array();
      $row->tag_names = array();
      $keys[$row->hamster_source_id][] = $row->fact_id;
    }

    foreach ($keys as $hamster_source_id => $fact_ids) {
      $q = db_select('hamster_fact_tags', 'ft');
      $q->leftJoin('hamster_tags', 't', 'ft.tag_id = t.id AND ft.hamster_source_id = t.hamster_source_id');
      $q->addField('t', 'name', 'tag_name');
      $q->fields('t');
      $q->addField('ft', 'tag_id');
      $q->addField('ft', 'fact_id');
      $q->condition('ft.fact_id', $fact_ids);
      $q->condition('ft.hamster_source_id', $hamster_source_id);
      foreach ($q->execute()->fetchAll() as $tag) {
        $key = $hamster_source_id . '.' . $tag->fact_id;
        if (!empty($all[$key])) {
          $all[$key]->tags[$tag->id] = $tag;
          $all[$key]->tag_names[$tag->id] = $tag->name;
        }
      }
    }
  }
}
