<?php

class hamster_Db_QueryBuilder {

  protected $metaJoiner;
  protected $conditionBuilder;
  protected $aggregateBuilder;

  function __construct() {
  }

  function select($type, $params = array()) {
    $type = hamster_Util::pluralOrNull($type);
    $this->conditionBuilder = new hamster_Db_QueryBuilder_ConditionBuilder($params);
    $this->metaJoiner = new hamster_Db_QueryBuilder_MetaJoiner($this->conditionBuilder);
    $this->aggregateBuilder = new hamster_Db_QueryBuilder_AggregateBuilder($this->conditionBuilder);
    if (!empty($type)) {
      $q = $this->$type($q_aggregate);
    }
    else {
      return $this->selectAggregate(NULL);
    }
    $this->metaJoiner->joinMeta($q, $type);
    $q->orderBy('latest', 'DESC');
    return $q;
  }

  protected function selectAggregate($type) {
    return $this->aggregateBuilder->select($type);
  }

  protected function facts() {
    $q = $this->selectAggregate('facts');
    $q->addExpression("CONCAT(f.hamster_source_id, '.', f.id)", '_primary');
    $this->conditionBuilder->fieldAndCond($q, 'f', 'hamster_source_id');
    $q->addField('f', 'id');
    return $q;
  }

  protected function activities() {
    $q = $this->build('activities');
    $q->addExpression("CONCAT(a.hamster_source_id, '.', a.id)", '_primary');
    $this->conditionBuilder->fieldAndCond($q, 'a', 'hamster_source_id');
    $q->addField('a', 'id');
    return $q;
  }

  protected function categories() {
    $q = $this->build('categories');
    $q->addExpression("CONCAT(c.hamster_source_id, '.', c.id)", '_primary');
    $this->conditionBuilder->fieldAndCond($q, 'c', 'hamster_source_id');
    $q->addField('c', 'id');
    return $q;
  }

  protected function tags() {
    $q = $this->build('tags');
    $q->addExpression("CONCAT(t.hamster_source_id, '.', t.id)", '_primary');
    $this->conditionBuilder->fieldAndCond($q, 't', 'hamster_source_id');
    $q->addField('t', 'id');
    return $q;
  }

  protected function sources() {
    $q = $this->build('sources');
    $q->addExpression('s.id', '_primary');
    $q->addField('s', 'id', 'hamster_source_id');
    $q->addField('s', 'id');
    return $q;
  }

  protected function build($type) {
    $table = 'hamster_' . $type;
    $alias = $type{0};
    $q = db_select($table, $alias)->fields($alias);
    $q_aggregate = $this->selectAggregate($type);
    $this->aggregateBuilder->joinAgg($q, $alias, $q_aggregate);
    $q->fields('agg');
    return $q;
  }
}

