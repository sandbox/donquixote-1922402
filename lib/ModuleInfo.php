<?php


class hamster_ModuleInfo {

  protected $menuInfo;

  function __construct($menu_info) {
    $this->menuInfo = $menu_info;
  }

  function hook_perm() {
    return array(
      'view hamster timetracking data',
      'import hamster timetracking data',
    );
  }

  function hook_theme() {
    return array(
      'hamster_associated_nid_column' => array(),
    );
  }

  function hook_menu() {
    $items = $this->menuInfo->hook_menu();
    $items['hamster/import'] = array(
      'type' => MENU_CALLBACK,
      'access arguments' => array('import hamster timetracking data'),
      'page callback' => 'hamster_import_page',
      'file' => 'rest/hamster.import.inc',
    );
    $items['hamster/import/post'] = array(
      'type' => MENU_CALLBACK,
      'access callback' => TRUE,
      'page callback' => 'hamster_import_post',
      'file' => 'rest/hamster.import.inc',
    );
    return $items;
  }
}
