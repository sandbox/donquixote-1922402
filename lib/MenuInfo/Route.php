<?php

class hamster_MenuInfo_Route implements ArrayAccess {

  protected $route;
  protected $fragments;

  function __construct($route) {
    $this->route = $route;
    $this->fragments = explode('/', $route);
  }

  function __toString() {
    return $this->route;
  }

  function offsetExists($i) {
    return isset($this->fragments[$i]);
  }

  function offsetGet($i) {
    return isset($this->fragments[$i]) ? $this->fragments[$i] : NULL;
  }

  function offsetSet($i, $value) {
    throw new Exception("A Route object cannot be modified.");
  }

  function offsetUnset($i) {
    throw new Exception("A Route object cannot be modified.");
  }
}
