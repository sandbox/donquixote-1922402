<?php

class hamster_MenuInfo_LinkBuilder {

  function l($title, $path, $options = array()) {
    $this->modifyLinkOptions($options);
    return l($title, $path, $options);
  }

  function url($path, $options = array()) {
    $this->modifyLinkOptions($options);
    return url($path, $options);
  }

  function modifyLinkOptions(&$options) {
    // Preserve the time parameters.
    $params = $_GET;
    unset($params['q']);
    if (!empty($params)) {
      if (empty($options['query'])) {
        $options['query'] = $params;
      }
      elseif (!is_array($options['query'])) {
        $options['query'] .= '&' . drupal_query_string_encode($params);
      }
      else {
        $options['query'] += $params;
      }
    }
  }

  function userLink($obj, $type) {
    $path = 'user/' . $obj->uid . '/hamster';
    if (!empty($type)) {
      $path .= '/' . hamster_Util::plural($type);
    }
    return $this->l($obj->username, $path);
  }

  function sourceLink($obj, $type) {
    if ($type === 'source') {
      return '<strong>' . $obj->name . '</strong>';
    }
    else {
      $path = 'hamster/' . $obj->hamster_source_id;
      if (!empty($type)) {
        $path .= '/' . hamster_Util::plural($type);
      }
      return $this->l($obj->source_name, $path);
    }
  }

  function relatedObjectLink($related_type, $obj, $type) {
    if ($related_type === $type) {
      return '<strong>' . $obj->name . '</strong>';
    }
    else {
      $related_id_key = $related_type . '_id';
      $related_name_key = $related_type . '_name';
      $related_plural = hamster_Util::plural($related_type);
      $path = 'hamster/' . $obj->hamster_source_id . '/' . $related_plural . '/' . $obj->$related_id_key;
      if (!empty($type)) {
        $path .= '/' . hamster_Util::plural($type);
      }
      return $this->l($obj->$related_name_key, $path);
    }
  }

  function nodeLink($assoc_type, $obj, $object_type) {
    $nid = $obj->{$assoc_type . '_nid'};
    $title = $obj->{$assoc_type . '_node_title'};
    return l($title, 'node/' . $nid . '/hamster/' . hamster_Util::plural($object_type));
  }
}
