<?php


class hamster_MenuInfo_PageHelper {

  protected $services;
  protected $collections;
  protected $linkBuilder;
  protected $loader;
  protected $dynamicParams = array();

  function __construct($services) {
    $this->services = $services;
    $this->collections = $services->collections;
    $this->linkBuilder = $services->linkBuilder;
    $this->loader = $services->loader;
    if (!empty($_GET['date_begin'])) {
      $this->dynamicParams['time_begin'] = strtotime($_GET['date_begin']);
    }
    if (!empty($_GET['date_end'])) {
      $this->dynamicParams['time_end'] = strtotime($_GET['date_end']);
    }
  }

  /**
   * Show an info box about a source, category, activity, etc.
   */
  function objectSummary($type, $obj, $link_builder = NULL) {
    $b = !empty($link_builder) ? $link_builder : $this->linkBuilder;
    $rows = array();
    if (isset($obj->username)) {
      $rows[] = array('User:', $b->userLink($obj, $type));
      if (isset($obj->source_name)) {
        $rows[] = array('Source:', $b->sourceLink($obj, $type));
        if (isset($obj->tag_name)) {
          $rows[] = array('Tag:', $b->relatedObjectLink('tag', $obj, $type));
        }
        if (isset($obj->category_name)) {
          $rows[] = array('Category:', $b->relatedObjectLink('category', $obj, $type));
          if (isset($obj->activity_name)) {
            $rows[] = array('Activity:', $b->relatedObjectLink('activity', $obj, $type));
          }
        }
      }
    }
    if (isset($obj->category_node_title)) {
      $rows[] = array('Project:', $b->nodeLink('category', $obj, $type));
    }
    // We want a table without the striping..
    $html = '';
    foreach ($rows as $cells) {
      $row_html = '';
      foreach ($cells as $cell) {
        $row_html .= '<td>' . $cell . '</td>';
      }
      $html .= '<tr>' . $row_html . '</tr>';
    }
    return '<table style="width:auto; margin-right:auto;"><tbody>' . $html . '</tbody></table>';
  }

  /**
   * Show a list of objects
   */
  function listPage($type, $params, $multicrud = FALSE) {
    $params = $this->_buildParams($params);
    $type = hamster_Util::plural($type);
    $html = '';
    $html .= $this->filterForm();
    if ($multicrud) {
      return $html . $this->multicrudPage($type, $params);
    }
    else {
      return $html . $this->renderList($type, $params);
    }
  }

  /**
   * Show a list of objects with editable fields.
   */
  protected function multicrudPage($type, $params) {
    return __METHOD__;
  }

  /**
   * Show a form to filter the date.
   */
  protected function filterForm() {
    module_load_include('pages.inc', 'hamster');
    return drupal_get_form('hamster_filter_form');
  }

  
  protected function _buildParams($params) {
    return $params + $this->dynamicParams;
  }

  function monthsAggregateList($orig_params, $base_path) {
    // Table definition.
    $list_class = 'hamster_List_Periodic';
    $list = new $list_class($orig_params, $this->linkBuilder);
    $header = $list->header();
    $all = $this->collections->fetchAllPeriodic('months', NULL, $orig_params);
    $rows = $list->rows($all, FALSE);
    return theme('table', $header, $rows);
  }

  function renderMonthsList($type, $orig_params, $month, $n_months) {
    // Table definition.
    $list_class = 'hamster_List_' . ucfirst($type);
    $list = new $list_class($params, $this->linkBuilder);
    $header = $list->header();
    $colspan = $list->colspan();
    $rows = array();
    for ($i = 0; $i < $n_months; ++$i) {
      $params = $this->buildMonthParams($orig_params, $month);
      $all = $this->db->fetchAll($type, $params);
      $rows[][] = array(
        'colspan' => $colspan,
        'data' => ucfirst($type) . ' in ' . date('F Y', $month->time_begin),
        'header' => TRUE,
      );
      $rows = array_merge($rows, $list->rows($all, FALSE));
      $month = $this->prevMonth($month);
    }
    return theme('table', $header, $rows);
  }

  function renderMonths($type, $month, $n_months) {
    $list_class = 'hamster_List_' . ucfirst($type);
    $list = new $list_class($params, $this->linkBuilder);
    $header = $list->header();
  }

  protected function buildMonthParams($orig_params, $month) {
    return $orig_params + array(
      'month' => $month,
      'time_begin' => $month->time_begin,
      'time_end' => $month->time_end,
    );
  }

  function renderList($type, $params) {
    // Table definition.
    $list_class = 'hamster_List_' . ucfirst($type);
    $list = new $list_class($params, $this->linkBuilder);
    $header = $list->header();
    $all = $this->collections->fetchAll($type, $params);
    $rows = $list->rows($all, TRUE);
    return theme('table', $header, $rows);
  }
}
