<?php

class hamster_List_Categories extends hamster_List_Abstract {

  protected function columns() {
    $header = array();
    $header['source'] = 'User/machine';
    $header['category_node'] = 'Project';
    $header['category'] = 'Category';
    $header['earliest'] = 'Earliest';
    $header['latest'] = 'Latest';
    $header['n_activities'] = 'Act.';
    $header['n_facts'] = 'Facts';
    $header['duration'] = 'Hours';
    return $header;
  }

  protected function rowCells($row) {
    $cells = parent::rowCells($row);
    return $cells;
  }
}
