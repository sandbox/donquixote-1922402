<?php


class hamster_List_Facts extends hamster_List_Abstract {

  protected function columns() {
    $header = array();
    $header['user'] = 'User';
    $header['activity'] = 'Activity';
    $header['category'] = 'Category';
    $header['description'] = 'Description';
    $header['earliest'] = 'Earliest';
    $header['latest'] = 'Latest';
    $header['duration'] = 'Hours';
    // $header['n_tags'] = '# Tags';
    return $header;
  }

  protected function rowCells($row) {
    $cells = parent::rowCells($row);
    $cells['description'] = $row->description;
    $tags_html = array();
    foreach ($row->tags as $tag) {
      $tags_html[] = $this->l($tag->name, 'hamster/' . $tag->hamster_source_id . '/tags/' . $tag->id);
    }
    if (!empty($tags_html)) {
      $cells['description'] .= '<div>' . implode(', ', $tags_html) . '</div>';
    }
    return $cells;
  }
}
