<?php

class hamster_List_Abstract {

  protected $params;
  protected $linkBuilder;

  function __construct($params, $link_builder) {
    if (!is_object($link_builder)) {
      throw new Exception("Link builder must be an object. $link_builder.");
    }
    $this->params = $params;
    $this->linkBuilder = $link_builder;
  }

  function header() {
    return array_values($this->visibleColumns());
  }

  protected function l($title, $path, $options = array()) {
    return $this->linkBuilder->l($title, $path, $options);
  }

  protected function visibleColumns() {
    $columns = $this->columns();
    foreach (array(
      array('uid', 'user'),
      array('nid', 'node'),
      array('hamster_source_id', 'source'),
      array('hamster_source_id', 'user'),
      array('category_id', 'category'),
      array('activity_id', 'activity'),
      array('activity_id', 'category'),
    ) as $info) {
      list($param_key, $column_key) = $info;
      if (isset($this->params[$param_key])) {
        unset($columns[$column_key]);
      }
    }
    return $columns;
  }

  protected function columns() {
    $header = array();
    $header['earliest'] = 'Earliest';
    $header['latest'] = 'Latest';
    $header['duration'] = 'Hours';
    return $header;
  }

  function colspan() {
    return count($this->visibleColumns());
  }

  function rows($all, $grouped = FALSE) {
    $columns = $this->visibleColumns();
    $n_cols = count($columns);
    $group_key = NULL;
    $rows = array();
    foreach ($all as $row) {
      if ($grouped) {
        $new_group_key = $this->rowGroupKey($row);
        if ($new_group_key !== $group_key) {
          $group_key = $new_group_key;
          $rows[] = $this->groupHeader($group_key, $n_cols);
        }
      }
      $rows[] = $this->rowCells($row);
    }
    foreach ($rows as $i => $row) {
      $rows[$i] = array();
      if (is_string($row)) {
        $rows[$i][] = array(
          'colspan' => $n_cols,
          'data' => $row,
          'header' => TRUE,
        );
      }
      else {
        foreach ($columns as $k => $v) {
          if (isset($row[$k])) {
            $rows[$i][] = $row[$k];
          }
          else {
            $rows[$i][] = '';
          }
        }
      }
    }
    return $rows;
  }

  protected function rowCells($row) {
    $cells = array();
    $cells['earliest'] = $this->dateIfSet('Y-m-d', $row, 'earliest');
    $cells['latest'] = $this->dateIfSet('Y-m-d', $row, 'latest');
    $cells['duration'] = $this->durationIfSet($row);
    $cells['n_activities'] = $this->intIfSet($row, 'n_activities');
    $cells['n_facts'] = $this->intIfSet($row, 'n_facts');
    $cells['n_tags'] = $row->n_tags . ' tags';
    $cells['user'] = $this->l($row->username, 'user/' . $row->uid . '/hamster');
    if (isset($row->source_name) && isset($row->hamster_source_id)) {
      $cells['source'] = $this->l($row->source_name, 'hamster/' . $row->hamster_source_id);
      if (isset($row->category_name) && isset($row->category_id)) {
        $cells['category'] = $this->l($row->category_name, 'hamster/' . $row->hamster_source_id . '/categories/' . $row->category_id);
        if (isset($row->activity_name) && isset($row->activity_id)) {
          $cells['activity'] = $this->l($row->activity_name, 'hamster/' . $row->hamster_source_id . '/activities/' . $row->activity_id);
        }
      }
    }
    if (isset($row->node_title) && isset($row->associated_nid)) {
      $cells['node'] = $this->l($row->node_title, 'node/' . $row->associated_nid . '/hamster');
    }
    if (isset($row->category_node_title)) {
      $cells['category_node'] = $this->l($row->category_node_title, 'node/' . $row->category_nid . '/hamster');
    }
    if (isset($row->activity_node_title)) {
      $cells['activity_node'] = $this->l($row->activity_node_title, 'node/' . $row->activity_nid . '/hamster');
    }
    if (isset($row->fact_node_title)) {
      $cells['fact_node'] = $this->l($row->fact_node_title, 'node/' . $row->fact_nid . '/hamster');
    }
    return $cells;
  }

  protected function rowGroupKey($row) {
    return date('Y', $row->latest);
  }

  protected function groupHeader($group_key, $n_cols) {
    return "Last updated in $group_key";
  }

  protected function linkIfCount(&$cells, $type, $path) {
    $key = "n_$type";
    if (!empty($cells[$key]) && is_numeric($cells[$key])) {
      $cells[$key] = l($cells[$key] . ' ' . $type, $path);
    }
    else {
      $cells[$key] = '-';
    }
  }

  protected function intIfSet($row, $key) {
    return isset($row->$key) ? $row->$key : '';
  }

  protected function dateIfSet($format, $row, $key) {
    if (isset($row->$key)) {
      return date($format, $row->$key);
    }
    else {
      return '-';
    }
  }

  protected function durationIfSet($row) {
    if (isset($row->duration)) {
      return number_format($row->duration / 60. / 60, 2);
    }
    else {
      return '-';
    }
  }
}
