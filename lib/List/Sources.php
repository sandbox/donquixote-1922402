<?php


class hamster_List_Sources extends hamster_List_Abstract {

  protected function columns() {
    $header = array();
    $header['user'] = 'User';
    $header['source'] = 'Source';
    $header['earliest'] = 'Earliest';
    $header['latest'] = 'Latest';
    $header['n_categories'] = 'Cat.';
    $header['n_activities'] = 'Act.';
    $header['n_facts'] = 'Facts';
    $header['duration'] = 'Hours';
    return $header;
  }

  protected function rowCells($row) {
    $cells = parent::rowCells($row);
    $cells['source'] = $row->name;
    $cells['n_categories'] = $row->n_categories;
    $cells['n_activities'] = $row->n_activities;
    return $cells;
  }
}
