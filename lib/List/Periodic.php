<?php


class hamster_List_Periodic extends hamster_List_Abstract {

  protected $periodType;
  protected $basePath;

  function __construct($params, $link_builder, $period_type = 'month') {
    parent::__construct($params, $link_builder);
    if ($period_type !== 'month') {
      throw new Exception("Period type not supported.");
    }
    $this->periodType = $period_type;
    if (isset($params['category_nid'])) {
      $this->basePath = 'node/' . $params['category_nid'] . '/hamster';
    }
  }

  protected function columns() {
    $header = array();
    switch ($this->periodType) {
      case 'month':
        $header['year'] = 'Year';
        $header['month'] = 'Month';
        break;
      default:
        throw new Exception("Period type not supported.");
    }
    $header['n_categories'] = 'Cat.';
    $header['n_activities'] = 'Act.';
    $header['n_facts'] = 'Facts';
    $header['duration'] = 'Hours';
    return $header;
  }

  protected function rowCells($row) {
    if (!isset($this->basePath)) {
      throw new Exception("Must set base path before rendering this list.");
    }
    $cells = parent::rowCells($row);
    switch ($this->periodType) {
      case 'month':
        $cells['month'] = date('F', $row->period_begin);
        $cells['year'] = date('Y', $row->period_begin);
    }
    $cells['source'] = $row->name;
    $base = $this->basePath . '/' . $row->period->slug;
    $this->linkIfCount($cells, 'categories', $base . '/categories');
    $this->linkIfCount($cells, 'activities', $base . '/activities');
    $this->linkIfCount($cells, 'facts', $base . '/facts');
    return $cells;
  }

  protected function rowGroupKey($row) {
    $slug = date('Y-m', $row->period_begin);
    list($year, $month) = explode('-', $slug);
    if ($month > 6) {
      return "$year, July - December";
    }
    else {
      return "$year, January - June";
    }
    $year = (int)$year;
    $last_year = $year - 1;
    $next_year = $year + 1;
    $month = (int)$month;
    switch (floor($month / 3)) {
      case 0:
        return "Winter $last_year / $year";
      case 1:
        return "Spring $year";
      case 2:
        return "Summer $year";
      case 3:
        return "Autumn $year";
      case 4:
        return "Winter $year / $next_year";
    }
  }

  protected function groupHeader($group_key, $n_cols) {
    return $group_key;
  }
}
