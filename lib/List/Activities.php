<?php


class hamster_List_Activities extends hamster_List_Abstract {

  protected function columns() {
    $header = array();
    $header['user'] = 'User';
    $header['activity'] = 'Activity';
    $header['earliest'] = 'Earliest';
    $header['latest'] = 'Latest';
    $header['n_facts'] = 'Facts';
    $header['duration'] = 'Duration';
    return $header;
  }

  protected function rowCells($row) {
    $cells = parent::rowCells($row);
    if (!isset($this->params['category_id'])) {
      $cells['activity'] = $cells['activity'] . '@' . $cells['category'];
    }
    return $cells;
  }
}
