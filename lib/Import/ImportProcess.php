<?php

class hamster_Import_ImportProcess {

  protected $sourceId;
  protected $logHandler;
  protected $sqlite;

  function __construct($source_id, $log_handler = NULL) {
    $this->sourceId = $source_id;
    $this->logHandler = $log_handler;
    $this->sqlite = new hamster_Import_SqLite();
  }

  function importDir($dir) {
    $this->_log("\nImport dir '$dir'", 'ok');
    foreach (scandir($dir) as $candidate) {
      if ($candidate == '..' || $candidate == '.' || !is_file($dir . '/' . $candidate)) {
        continue;
      }
      $this->importFile($dir . '/' . $candidate);
    }
  }

  function importFile($filename) {
    $this->_log("\nImport file '$filename'", 'ok');
    $db = $this->sqlite->open($filename);
    $q = $db->query("SELECT * FROM sqlite_master");
    if (!$q) {
      throw new Exception("Failed to read '$filename' as an sqlite database.");
    }
    while ($source_tableinfo = $q->fetchObject()) {
      $dest_tableinfo = drupal_get_schema('hamster_' . $source_tableinfo->name);
      if ($dest_tableinfo) {
        $this->_importTable($db, $source_tableinfo, $dest_tableinfo);
      }
    }
  }

  protected function _importTable($db, $source_tableinfo, $dest_tableinfo) {
    $source_tablename = $source_tableinfo->name;
    $q = $db->query("SELECT * FROM $source_tablename");
    $rows = array();
    while ($row = $q->fetchObject()) {
      $row->hamster_source_id = $this->sourceId;
      $primary = $this->getPrimary($row);
      if (isset($row->start_time)) {
        $row->start_time = strtotime($row->start_time);
      }
      if (isset($row->end_time)) {
        $row->end_time = strtotime($row->end_time);
      }
      $row->deleted = 0;
      $rows[$primary] = $row;
    }
    $this->_writeRows($rows, $dest_tableinfo);
  }

  protected function _writeRows($rows, $dest_tableinfo) {
    $dest_tablename = $dest_tableinfo['name'];

    // Check which of the rows already exist.
    $update = array();
    $insert = $rows;
    $delete = array();

    $q = db_select($dest_tablename, 'x')->fields('x');
    $q->condition('hamster_source_id', $this->sourceId);
    hamster_dpq($q);
    foreach ($q->execute() as $dest_row) {
      $primary = $this->getPrimary($dest_row);
      if (isset($rows[$primary])) {
        $update[$primary] = $rows[$primary];
        unset($insert[$primary]);
      }
      else {
        // do not delete anything! this row could be in another file somewhere.
        foreach ($dest_tableinfo['primary key'] as $k) {
          $dest_row->deleted = TRUE;
          $delete[$primary] = $dest_row;
        }
      }
    }
    dpm($dest_tableinfo);

    // update
    foreach ($update as $primary => $row) {
      drupal_write_record($dest_tablename, $row, $dest_tableinfo['primary key']);
    }

    // insert
    foreach ($insert as $primary => $row) {
      drupal_write_record($dest_tablename, $row);
    }

    // delete
    foreach ($delete as $primary => $row) {
      // just set the "deleted" bit.
      drupal_write_record($dest_tablename, $row, $dest_tableinfo['primary key']);
    }

    $n_update = count($update);
    $n_insert = count($insert);
    $n_delete = count($delete);

    $this->_log("Table '$dest_tablename': $n_update rows updated, $n_insert rows inserted, $n_delete rows moved to trash.", 'ok');
  }

  protected function getPrimary($row) {
    return $row->hamster_source_id . '.' . $row->id;
  }

  protected function _log($msg, $type = 'notice', $error = NULL) {
    if (is_object($this->logHandler)) {
      $this->logHandler->log($msg, $type, $error);
    }
    else if (is_callable($this->logHandler)) {
      $f = $this->logHandler;
      $f($msg, $type, $error);
    }
    else {
      // do nothing..
    }
  }
}
