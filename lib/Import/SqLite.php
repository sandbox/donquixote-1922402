<?php

class hamster_Import_SqLite {

  /**
   * Open a sqlite PDO connection object.
   */
  function open($file) {
    if (!class_exists('PDO')) {
      throw new Exception("Class 'PDO' is undefined.");
    }
    $db = new PDO('sqlite:' . $file);
    return $db;
  }
}
