<?php

class hamster_Import_ChallengeAPI {

  protected $uid;

  function __construct() {
    if (!empty($GLOBALS['user']->uid)) {
      $this->uid = (int)$GLOBALS['user']->uid;
    }
  }

  function generatePersonal() {
    return $this->generate($GLOBALS['user']->uid);
  }

  function generate($uid = NULL) {
    $challenge = hamster_Util::randomString();
    db_insert('hamster_api_challenge')->fields(array(
      'challenge' => $challenge,
      'created' => time(),
      'uid' => $uid,
    ))->execute();
    return $challenge;
  }

  function checkFileImport($challenge, $source_name, $file, $challenge_response) {

    if (!$obj = $this->consume($challenge)) {
      // Invalid challenge.
      return FALSE;
    }

    if (!empty($obj->uid)) {
      // For logged-in users, the challenge works like a CSRF token.
      // No further validation required.
      return TRUE;
    }

    $api_key = $this->sourceApiKey($source_name);
    if (empty($api_key)) {
      // Anonymous users need an API key.
      return FALSE;
    }

    $hash = md5($ser = serialize($data = array(
      'challenge' => $challenge,
      'source_name' => $source_name,
      'api_key' => $api_key,
      'file_hash' => hash_file('md5', $file),
    )));

    if ($hash !== $challenge_response) {
      print "mismatch in checkFileImport\n  $hash !== $challenge_response\n";
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  protected function sourceApiKey($source_name) {
    $q = db_select('hamster_sources', 's');
    $q->addField('s', 'api_key');
    $q->condition('s.name', $source_name);
    return $q->execute()->fetchField();
  }

  protected function consume($challenge) {
    $q = db_select('hamster_api_challenge', 'ach');
    $q->fields('ach', array('created', 'uid'));
    $q->condition('ach.challenge', $challenge);
    $obj = $q->execute()->fetchObject();
    if (!empty($obj)) {
      if (!empty($obj->uid) && (int)$obj->uid !== $this->uid) {
        // May not touch a challenge from another user.
        return FALSE;
      }
      // Delete the one-time challenge.
      $q = db_delete('hamster_api_challenge');
      $q->condition('challenge', $challenge);
      $q->execute();
      // Check that it's not older than 20 minutes.
      if ($obj->created + 20 * 60 > time()) {
        return $obj;
      }
    }
    return FALSE;
  }
}
