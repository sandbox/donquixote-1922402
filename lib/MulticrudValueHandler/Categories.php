<?php


/**
 * The value handler is responsible for loading, saving and validation of items.
 */
class hamster_MulticrudValueHandler_Categories extends multicrud_ValueHandler {

  protected $_filterParams;
  protected $_originalItems;
  protected $_linkOptions;

  /**
   * @param $type
   *   the node type
   * @param $langSpecificFields
   *   names of fields that are not shared across languages
   */
  function __construct(array $filter_params) {
    $this->_filterParams = $filter_params;
    $this->_originalItems = hamster('db')->fetchAll('categories', $filter_params);
    $this->_linkOptions = array();
    foreach (array('date_begin', 'date_end') as $key) {
      if (!empty($filter_params[$key])) {
        $this->_linkOptions['query'][$key] = $filter_params[$key];
      }
    }
    module_load_include('inc', 'hamster', 'hamster.db');
  }

  function validateItem($item, $meta, $error_handler) {
    // TODO: Implement item validation
  }

  function getDefaultValue() {
    $items = $this->_originalItems;
    foreach ($items as $k => &$item) {
      $link_path = 'hamster/' . $item['hamster_source_id'] . '/categories/' . $item['id'];
      $item['category'] = l($item['name'], $link_path, $this->_linkOptions);
      $item['source'] = l($item['source_name'], 'hamster/' . $item['hamster_source_id'], $this->_linkOptions);
      $item['count'] = $item['n_activities'] . ' / ' . $item['n_facts'];
      $item['n_activities'] = l((int)$item['n_activities'], $link_path, $this->_linkOptions);
      if ($item['n_facts'] > 0) {
        $item['duration'] = number_format($item['duration'] / 60. / 60, 2);  // . ' h';
        $item['earliest'] = date('Y-m-d', $item['earliest']);
        $item['latest'] = date('Y-m-d', $item['latest']);
      }
      else {
        $item['duration'] = '-';
        $item['earliest'] = '-';
        $item['latest'] = '-';
      }
      if ($item['associated_nid']) {
        $item['associated_nid'] = $item['node_title'] . ' [nid:' . $item['associated_nid'] . ']';
      }
    }
    return $items;
  }

  /**
   * Called from multicrud for to-be-updated items.
   */
  function updateItem($id, $item, $meta) {
    if (!$this->_validateItem($item)) {
      return FALSE;
    }
    if (!isset($this->_originalItems[$id])) {
      // we cannot update something that does not exist.
      return;
    }
    $original_item = $this->_originalItems[$id];
    if (preg_match('#^[1-9][0-9]*$#', trim($item['associated_nid']), $m)) {
      $original_item['associated_nid'] = (int)$m[0];
    }
    else if (preg_match('#\[nid: *([1-9][0-9]*)\]$#', trim($item['associated_nid']), $m)) {
      $original_item['associated_nid'] = (int)$m[1];
    }
    else if (trim($item['associated_nid']) === '') {
      $original_item['associated_nid'] = NULL;
    }
    $success = _hamster_write_record('hamster_categories', $original_item, array('id', 'hamster_source_id'));
    return $success;
  }

  protected function _validateItem($item) {
    return TRUE;
  }
}
