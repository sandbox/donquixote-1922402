<?php

class hamster_WildcardLoader {

  protected $collections;
  protected $cache = array();

  function __construct($collections) {
    $this->collections = $collections;
  }

  function currentMonth() {
    return $this->loadMonth(time());
  }

  function loadMonth($time) {
    $slug = date('Y-m', $time);
    return $this->hamster_month($slug);
  }

  function previousMonth($month) {
    $prevmonth_year = $month->year;
    $prevmonth_month = $month->month - 1;
    hamster_Util::balanceMonth($prevmonth_year, $prevmonth_month);
    return $this->hamster_month("$prevmonth_year-$prevmonth_month");
  }

  function hamster_month($month_slug) {
    if (!isset($this->cache['month'][$month_slug])) {
      if (preg_match('/^(\d\d\d\d)-(\d\d|\d)$/', $month_slug, $m)) {
        $year = (int)$m[1];
        $month = (int)$m[2];
        hamster_Util::balanceMonth($year, $month);
        $nextmonth_month = $month + 1;
        $nextmonth_year = $year;
        hamster_Util::balanceMonth($nextmonth_year, $nextmonth_month);
        $time = strtotime("$year/$month/01 00:00");
        $obj = (object)array(
          'original_slug' => $month_slug,
          'slug' => date('Y-m', $time),
          'year' => $year,
          'month' => $month,
          'time_begin' => strtotime("$year/$month/01 00:00"),
          'time_end' => strtotime("$nextmonth_year/$nextmonth_month/01 00:00"),
        );
        $obj->duration = $obj->time_end - $obj->time_begin;
        $obj->n_days = $obj->duration / (60. * 60 * 24);
        $obj->begin = date('Y-m-d H:i', $obj->time_begin);
        $obj->end = date('Y-m-d H:i', $obj->time_end);
        $this->cache['month'][$month_slug] = $obj;
      }
      $this->cache['month'][$month_slug] = isset($obj) ? $obj : FALSE;
    }
    return $this->cache['month'][$month_slug];
  }

  function hamster_object($type, $source_id, $id) {
    if (!isset($this->cache[$type][$source_id][$id])) {
      $obj = $this->collections->fetchOne($type, array(
        'hamster_source_id' => $source_id,
        $type . '_id' => $id,
      ));
      $this->cache[$type][$source_id][$id] = isset($obj) ? $obj : FALSE;
    }
    return $this->cache[$type][$source_id][$id];
  }

  function hamster_source($id_or_name) {
    if (is_numeric($id_or_name)) {
      return $this->sourceById($id_or_name);
    }
    else {
      return $this->sourceByName($id_or_name);
    }
  }

  function sourceById($id) {
    if (!isset($this->cache['source'][$id])) {
      $source = $this->collections->fetchOne('source', array(
        'hamster_source_id' => $id,
      ));
      $this->cache['source'][$id] = isset($source) ? $source : FALSE;
    }
    return $this->cache['source'][$id];
  }

  function sourceByName($name) {
    $id = $this->sourceIdByName($name);
    if (!empty($id)) {
      return $this->sourceById($id);
    }
  }

  function sourceIdByName($name) {
    $q = db_select('hamster_sources', 's');
    $q->addField('s', 'id');
    $q->condition('name', $name);
    return $q->execute()->fetchField();
  }
}
