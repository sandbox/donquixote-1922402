<?php


class hamster_ServiceFactory {

  function get_challenge($cache) {
    return new hamster_Import_ChallengeAPI();
  }

  function get_collections($cache) {
    return new hamster_Db_CollectionGateway($cache->queryBuilder);
  }

  function get_queryBuilder($cache) {
    return new hamster_Db_QueryBuilder();
  }

  function get_loader($cache) {
    return new hamster_WildcardLoader($cache->collections);
  }

  function get_info($cache) {
    return new hamster_ModuleInfo($cache->menuInfo);
  }

  function get_menuInfo($cache) {
    $plugins = array(
      // 'import' => new hamster_Route_ImportAPI(),
      'static' => new hamster_Route_StaticPages(),
      'admin' => new hamster_Route_AdminPages(),
      'source' => new hamster_Route_SourcePages(),
      'category' => new hamster_Route_HamsterPages('category'),
      'activity' => new hamster_Route_HamsterPages('activity'),
      'tag' => new hamster_Route_HamsterPages('tag'),
      'user' => new hamster_Route_UserPages(),
      'node' => new hamster_Route_NodePages($cache->nodeAssocInfo, $cache->collections),
      'project_month' => new hamster_Route_ProjectMonthPages($cache->nodeAssocInfo, $cache->collections),
      'autocomplete' => new hamster_Route_Autocomplete($cache->nodeAssocInfo),
    );
    return new hamster_MenuInfo($plugins, $cache->pageHelper);
  }

  function get_nodeAssocInfo($cache) {
    $info = array();
    foreach (array('activity', 'category', 'fact', 'tag') as $type) {
      $info[$type] = variable_get('hamster_settings__' . $type, array());
    }
    return new hamster_NodeAssocInfo($info);
  }

  function get_pageHelper($cache) {
    return new hamster_MenuInfo_PageHelper($cache);
  }

  function get_linkBuilder($cache) {
    return new hamster_MenuInfo_LinkBuilder();
  }
}
