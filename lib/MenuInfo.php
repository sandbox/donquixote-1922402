<?php


class hamster_MenuInfo {

  protected $plugins;
  protected $pageHelper;

  function __construct($plugins, $page_helper) {
    $this->plugins = $plugins;
    $this->pageHelper = $page_helper;
  }

  function hook_menu() {
    $items = array();
    foreach ($this->plugins as $plugin_name => $plugin) {
      foreach ($plugin->hook_menu() as $route => $item) {
        $this->_autoItemInfo($item, $plugin_name, $route);
        $items[$route] = $item;
      }
    }
    return $items;
  }

  protected function _autoItemInfo(&$item, $plugin_name, &$route) {
    $mini_fragments = array();
    $append_args = array();
    foreach (explode('/', $route) as $i => $fragment) {
      if ($fragment{0} === '%') {
        $append_args[] = $i;
        $mini_fragments[] = '%';
      }
      else {
        $mini_fragments[] = $fragment;
      }
    }
    $prepend_args = array($plugin_name, implode('/', $mini_fragments));
    if (count($append_args) > 1) {
      if (!isset($item['load arguments'])) {
        $item['load arguments'] = array($append_args[0]);
      }
    }
    $this->_autoCallbackInfo($item, $prepend_args, $append_args, 'page', 'hamster_page');
    $this->_autoCallbackInfo($item, $prepend_args, $append_args, 'access', 'hamster_page_access');
    if (!isset($item['title'])) {
      $this->_autoCallbackInfo($item, $prepend_args, $append_args, 'title', 'hamster_page_title');
    }
  }

  protected function _autoCallbackInfo(&$item, $prepend_args, $append_args, $k, $callback) {
    if (!isset($item["$k callback"])) {
      $item["$k callback"] = $callback;
      if (!isset($item["$k arguments"])) {
        $item["$k arguments"] = array_merge($prepend_args, $append_args);
      }
      else {
        $item["$k arguments"] = array_merge($prepend_args, $item["$k arguments"], $append_args);
      }
    }
  }

  function page($args) {
    drupal_set_title($this->_autoCallback(array('pageTitle', 'title'), $args));
    return $this->_autoCallback('page', $args);
  }

  function pageAccess($args) {
    return $this->_autoCallback('pageAccess', $args);
  }

  function linkTitle($args) {
    return $this->_autoCallback(array('linkTitle', 'title'), $args);
  }

  protected function _autoCallback($method, $args) {
    $plugin_name = array_shift($args);
    $plugin = $this->plugins[$plugin_name];
    if ($method === 'page') {
      if (method_exists($plugin, 'setPageHelper')) {
        $plugin->setPageHelper($this->pageHelper);
      }
      else {
        $class = get_class($plugin);
        throw new Exception("Plugin with class '$class' has no method setPageHelper()");
      }
      $route = $args[0];
      $candidate = 'page__' . $this->_autoMethodSuffix($route);
      if (method_exists($plugin, $candidate)) {
        array_shift($args);
        return call_user_func_array(array($plugin, $candidate), $args);
      }
    }
    $args[0] = new hamster_MenuInfo_Route($args[0]);
    if (is_array($method)) {
      $methods = $method;
      foreach ($methods as $method) {
        if (method_exists($plugin, $method)) {
          return call_user_func_array(array($plugin, $method), $args);
        }
      }
    }
    else {
      if (!method_exists($plugin, $method)) {
        throw new Exception("Plugin '$plugin_name' does not implement method '$method'.");
      }
      return call_user_func_array(array($plugin, $method), $args);
    }
  }

  protected function _autoMethodSuffix($route) {
    $fragments = explode('-', $route);
    $route = array_shift($fragments);
    foreach ($fragments as $fragment) {
      $route .= ucfirst($fragment);
    }
    return strtr($route, array('%' => 'x', '/' => '_'));
  }
}
