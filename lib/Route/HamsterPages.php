<?php

class hamster_Route_HamsterPages extends hamster_Route_AbstractListPage {

  protected $type;
  protected $listTypePlural;

  function __construct($type) {
    $this->type = $type;
    switch ($type) {
      case 'category':
        $this->listTypePlural = 'activities';
        break;
      case 'activity':
      case 'tag':
        $this->listTypePlural = 'facts';
        break;
    }
  }

  function hook_menu() {
    $items = array();
    $base = 'hamster/%hamster_source/' . hamster_Util::plural($this->type) . '/%hamster_object';
    $item = array(
      'load arguments' => array(1, $this->type),
    );
    $items[$base] = $item;
    $items[$base . '/list'] = $item + array(
      'title' => ucfirst($this->listTypePlural),
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );
    $items[$base . '/multicrud'] = $item + array(
      'title' => 'Bulk edit',
      'type' => MENU_LOCAL_TASK,
    );
    return $items;
  }

  function page($route, $source, $obj) {
    $params = array(
      $this->type . '_id' => $obj->id,
      'hamster_source_id' => $obj->hamster_source_id,
    );
    $html = '';
    $html .= $this->pageHelper->objectSummary($this->type, $obj);
    $html .= $this->pageHelper->listPage($this->listTypePlural, $params, $route[4] === 'multicrud');
    return $html;
  }

  function pageAccess($route, $source, $obj) {
    return user_access('view hamster timetracking data');
  }

  function linkTitle($route, $source, $obj) {
    return $obj->name;
  }

  function pageTitle($route, $source, $obj) {
    switch ($this->type) {
      case 'category':
        return 'Category: ' . $obj->category_name . ' | ' . $source->name;
      case 'activity':
        return 'Activity: ' . $obj->activity_name . '@' . $obj->category_name . ' | ' . $source->name;
      case 'tag':
        return 'Tag: ' . $obj->name;
    }
  }
}
