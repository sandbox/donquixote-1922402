<?php

class hamster_Route_Abstract {

  protected function addTabs(&$items, $base, array $tabs, $default = 0) {
    if (is_numeric($default)) {
      $default = $tabs[$default];
    }
    foreach (array_keys($tabs) as $weight => $k) {
      if (is_string($k)) {
        $suffix = $k;
        $title = $tabs[$k];
      }
      else {
        $suffix = $tabs[$k];
        $title = ucfirst($suffix);
      }
      $items[$base . '/' . $suffix] = array(
        'title' => $title,
        'type' => ($suffix === $default) ? MENU_DEFAULT_LOCAL_TASK : MENU_LOCAL_TASK,
        'weight' => $weight,
      );
    }
  }
}
