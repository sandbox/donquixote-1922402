<?php

class hamster_Route_UserPages extends hamster_Route_AbstractListPage {

  function hook_menu() {
    $items = array();
    $base = 'user/%user/hamster';
    $items[$base] = array(
      'title' => 'Hamster',
      'type' => MENU_LOCAL_TASK,
    );
    $this->addTabs($items, $base, array('sources', 'categories', 'activities', 'multicrud' => 'Edit sources'), 'categories');
    return $items;
  }

  function page($route, $user) {
    $params = array('uid' => $user->uid);
    switch ($route[3]) {
      case 'sources':
        return $this->pageHelper->listPage('sources', $params);
      case 'multicrud':
        return $this->pageHelper->listPage('sources', $params, TRUE);
      case 'activities':
        return $this->pageHelper->listPage('activities', $params);
      case 'categories':
      default:
        return $this->pageHelper->listPage('categories', $params);
    }
  }

  function pageAccess($route, $user) {
    return user_access('view hamster timetracking data');
  }
}
