<?php


class hamster_Route_AdminPages {

  function hook_menu() {
    $items = array();
    $items['admin/settings/hamster'] = array(
      'title' => 'Hamster',1
    );
    return $items;
  }

  function page($route) {
    module_load_include('admin.inc', 'hamster');
    return drupal_get_form('hamster_admin_form');
  }

  function pageAccess($route) {
    return user_access('administer site configuration');
  }
}
