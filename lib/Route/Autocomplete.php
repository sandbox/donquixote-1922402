<?php

class hamster_Route_Autocomplete {

  function hook_menu() {
    $items = array();

    foreach (array('category', 'activity', 'fact', 'tag') as $type) {
      $items['hamster/autocomplete/' . $type] = array();
      // Constrain by source
      $items['hamster/autocomplete/%hamster_source/' . $type] = array();
      if ($type !== 'category') {
        // Constrain by category
        $items['hamster/autocomplete/%hamster_source/%hamster_category/' . $type] = array();
      }
    }
    foreach ($items as $route => &$item) {
      $fragments = explode('/', $route);
      // Always assume an implicit last argument at the end.
      $n = count($fragments);
      $item['page arguments'] = array($n - 1, $n);
      // Access checking happens in the page callback, not in the access callback.
      $item['access callback'] = TRUE;
      // These are AJAX routes, they should not show up in menus, tabs or breadcrumbs.
      $item['type'] = MENU_CALLBACK;
      $item['title'] = FALSE;
    }
    return $items;
  }

  function page($route, $type, $search_string, $source = NULL, $obj = NULL) {
    module_load_include('autocomplete.inc', 'hamster');
    return hamster_autocomplete_json($type, $search_string, $source, $obj);
  }
}
