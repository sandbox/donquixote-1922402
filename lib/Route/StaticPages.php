<?php

class hamster_Route_StaticPages extends hamster_Route_AbstractListPage {

  function hook_menu() {
    $items = array();
    $items['hamster'] = array(
      'title' => 'Hamster',
    );
    $items['hamster/sources'] = array(
      'title' => 'Hamster sources',
    );
    return $items;
  }

  function page__hamster() {
    if (!empty($GLOBALS['user']->uid)) {
      drupal_goto('user/' . $GLOBALS['user']->uid . '/hamster');
    }
    drupal_goto('hamster/sources');
  }

  function page__hamster_sources() {
    $params = array();
    return __METHOD__;
  }

  function pageAccess($route) {
    return user_access('view hamster timetracking data');
  }
}
