<?php

class hamster_Route_NodePages extends hamster_Route_AbstractListPage {

  protected $nodeAssocInfo;

  function __construct($node_assoc_info) {
    $this->nodeAssocInfo = $node_assoc_info;
  }

  function hook_menu() {
    $items = array();
    $base = 'node/%node/hamster';
    $items[$base] = array(
      'title' => 'Hamster',
      'type' => MENU_LOCAL_TASK,
    );
    $this->addTabs($items, $base, array('months', 'categories', 'activities', 'facts'));
    return $items;
  }

  function page($route, $node) {
    $assoc_type = $this->nodeAssocInfo->nodeTypeToHamsterType($node->type);
    $list_type = $route[3] ? $route[3] : 'months';
    if (!empty($assoc_type)) {
      $params = array($assoc_type . '_nid' => $node->nid);
      if ($list_type === 'months') {
        return $this->pageHelper->monthsAggregateList($params, 'node/' . $node->nid . '/months');
      }
      else {
        return $this->pageHelper->listPage($list_type, $params);
      }
    }
  }

  function pageAccess($route, $node) {
    $assoc_type = $this->nodeAssocInfo->nodeTypeToHamsterType($node->type);
    switch ($route[3]) {
      case 'sources':
        if ($assoc_type === 'category') {
          return FALSE;
        }
      case 'categories':
        if ($assoc_type === 'activity') {
          return FALSE;
        }
      case 'activities':
        if ($assoc_type === 'fact') {
          return FALSE;
        }
      case 'months':
      default:
        if (empty($assoc_type)) {
          return FALSE;
        }
        return user_access('view hamster timetracking data');
    }
  }
}
