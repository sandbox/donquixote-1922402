<?php

class hamster_Route_SourcePages extends hamster_Route_AbstractListPage {

  function hook_menu() {
    $items = array();
    $base = 'hamster/%hamster_source';
    $items[$base] = array();
    $weight = 0;
    foreach (array(
      'categories' => 'Categories',
      'activities' => 'Activities',
      'tags' => 'Tags',
      'facts' => 'Facts',
    ) as $type => $title) {
      $items[$base . '/' . $type] = array(
        'title' => $title,
        'type' => $weight ? MENU_LOCAL_TASK : MENU_DEFAULT_LOCAL_TASK,
        'weight' => $weight++,
      );
      $items[$base . '/' . $type . '/list'] = array(
        'title' => 'List',
        'type' => MENU_DEFAULT_LOCAL_TASK,
        'weight' => 0,
      );
      $items[$base . '/' . $type . '/multicrud'] = array(
        'title' => 'Bulk edit',
        'type' => MENU_LOCAL_TASK,
        'weight' => 1,
      );
    }
    return $items;
  }

  function page($route, $source) {
    $type = isset($route[2]) ? $route[2] : 'categories';
    $params = array('hamster_source_id' => $source->id);
    $html = $this->pageHelper->objectSummary('source', $source);
    return $html . $this->pageHelper->listPage($type, $params, $route[3] === 'multicrud');
  }

  function pageAccess($route, $source) {
    return user_access('view hamster timetracking data');
  }

  function linkTitle($route, $source) {
    return $source->name;
  }

  function pageTitle($route, $source) {
    return 'Source: ' . $source->name;
  }
}
