<?php

class hamster_Route_ProjectMonthPages extends hamster_Route_AbstractListPage {

  protected $nodeAssocInfo;
  protected $db;

  function __construct($node_assoc_info, $collections) {
    $this->nodeAssocInfo = $node_assoc_info;
    $this->collections = $collections;
  }

  /**
   * Register menu/router paths.
   */
  function hook_menu() {
    $items = array();
    $base = 'node/%node/hamster/%hamster_month';
    $items[$base] = array();
    $this->addTabs($items, $base, array('categories', 'activities', 'facts'), 'activities');
    return $items;
  }

  /**
   * Page access callback
   */
  function pageAccess($route, $node, $month) {
    $assoc_type = $this->nodeAssocInfo->nodeTypeToHamsterType($node->type);
    if ($assoc_type === 'category') {
      return user_access('view hamster timetracking data');
    }
  }

  /**
   * Determine the title used in breadcrumbs and tabs.
   */
  function linkTitle($route, $node, $month) {
    return date('M Y', $month->time_begin + 1);
  }

  /**
   * Determine the title used on the page itself.
   */
  function pageTitle($route, $node, $month) {
    return date('F Y', $month->time_begin + 1) . ' at ' . $node->title;
  }

  /**
   * Build the page html for a given route.
   */
  function page($route, $node, $month) {
    $list_type = $route[4] ? $route[4] : 'activities';
    $params = array(
      'category_nid' => $node->nid,
      'time_begin' => $month->time_begin,
      'time_end' => $month->time_end,
    );
    $all = $this->collections->fetchAll($list_type, $params);
    return $this->renderList($all, $list_type, $params);
  }

  /**
   * Render a list.
   */
  protected function renderList($all, $list_type, $params) {
    $link_builder = new hamster_MenuInfo_LinkBuilder();
    $list_class = 'hamster_List_' . ucfirst($list_type);
    $list = new $list_class($params, $link_builder);
    $header = $list->header();
    $rows = $list->rows($all);
    return theme('table', $header, $rows);
  }
}
