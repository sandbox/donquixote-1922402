<?php

class hamster_PeriodRange_Months {

  function periods($earliest, $latest) {

    if (empty($latest)) {
      $latest = strtotime('now + 1 month');
    }
    if (empty($earliest)) {
      $earliest = strtotime('now - 5 years');
    }

    list($y_min, $m_min) = explode('-', date('Y-m', $earliest));
    list($y_max, $m_max) = explode('-', date('Y-m', $latest));
    $mm_min = $y_min * 12 + $m_min;
    $mm_max = $y_max * 12 + $m_max;

    $periods = array();
    $t_next = strtotime("2015-01-01 00:00");
    for ($y = $y_max; $y >= $y_min; --$y) {
      for ($m = 12; $m > 0; --$m) {
        $mm = $y * 12 + $m;
        if ($mm >= $mm_min && $mm <= $mm_max) {
          $t = strtotime("$y-$m-01 00:00");
          if (isset($t_next)) {
            $period = new stdClass;
            $period->time_end = $t_next;
            $period->time_begin = $t;
            $period->slug = date("Y-m", $period->time_begin);
            $periods[] = $period;
          }
        }
        $t_next = $t;
      }
    }
    return $periods;
  }
}
