<?php

class hamster_Debug_IndentedText {

  protected $text;
  protected $indent;

  function __construct(&$text, $indent = '') {
    $this->text =& $text;
    $this->indent = $indent;
  }

  function indent() {
    return new hamster_Debug_IndentedText($this->text, $this->indent . '  ');
  }

  function println($str = '') {
    $this->text .= "\n" . $this->indent;
    $this->pr($str);
  }

  function pr($str) {
    $this->text .= str_replace("\n", "\n" . $this->indent, $str);
  }

  function printList($list, $separator = ',') {
    if (is_string($list)) {
      $list = explode($separator . ' ', $list);
    }
    $this->println(implode("$separator\n", $list));
  }
}
