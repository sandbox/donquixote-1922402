<?php

class hamster_Util {

  static function plural($type) {
    if (!is_string($type)) {
      throw new Exception("\$type argument must be a string.");
    }
    $map = array(
      'activity' => 'activities',
      'category' => 'categories',
      'fact' => 'facts',
      'tag' => 'tags',
      'source' => 'sources',
    );
    return isset($map[$type]) ? $map[$type] : $type;
  }

  static function pluralOrNull($type) {
    if (!is_string($type)) {
      return NULL;
    }
    return self::plural($type);
  }

  static function balanceMonth(&$year, &$month) {
    if ($month > 12 || $month < 1) {
      // Note: PHP modulo sucks at negative numbers. So we use regular division.
      $str = "old: $year : $month";
      $n = floor(($month - 1) / 12);
      $year += $n;
      $month -= $n * 12;
    }
  }

  static function randomString($length = 49) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $str = '';
    for ($i = 0; $i < $length; $i++) {
      $str .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $str;
  }
}
