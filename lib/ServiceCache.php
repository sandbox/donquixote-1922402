<?php


class hamster_ServiceCache {

  protected $factory;
  protected $cache = array();

  function __construct($factory) {
    $this->factory = $factory;
  }

  function __get($key) {
    if (!isset($this->cache[$key])) {
      $method_to_call = 'get_' . $key;
      if (method_exists($this->factory, $method_to_call)) {
        $service = $this->factory->$method_to_call($this);
      }
      else {
        $service = $this->factory->get($this, $key);
      }
      $this->cache[$key] = isset($service) ? $service : FALSE;
    }
    return $this->cache[$key];
  }

  function __call($method, $args) {
    $key = serialize(array($method, $args));
    if (!isset($this->cache[$key])) {
      $method_to_call = 'call_' . count($args) . '_' . $method;
      if (method_exists($this->factory, $method_to_call)) {
        array_unshift($args, $this);
        $service = call_user_func_array(array($this->factory, $method_to_call), $args);
      }
      else {
        array_unshift($args, $method);
        array_unshift($args, $this);
        $service = call_user_func_array(array($this->factory, 'call'), $args);
      }
      $this->cache[$key] = isset($service) ? $service : FALSE;
    }
    return $this->cache[$key];
  }

  function reset($key) {
    unset($this->cache[$key]);
  }
}
