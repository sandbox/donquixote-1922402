<?php


function hamster_overview_page() {

  $select_sum = db_select('hamster_categories', 'c');
  $select_sum->addField('c', 'hamster_source_id');
  $select_sum->addExpression('COUNT(*)', 'n_categories');
  $select_sum->groupBy('c.hamster_source_id');

  $select = db_select('hamster_sources', 's');
  $select->fields('s');
  $select->leftJoin($select_sum, 'sum', 'sum.hamster_source_id = s.id');
  $select->fields('sum', array('n_categories'));

  $sources = $select->execute()->fetchAll(PDO::FETCH_CLASS);

  $result = array();
  foreach ($sources as $source) {
    $result[$source->uid][$source->id] = $source;
  }

  // foreach ($result as $uid => $
  if (!empty($GLOBALS['user']->uid)) {
    drupal_goto('user/' . $GLOBALS['user']->uid . '/hamster');
  }
  drupal_goto('hamster/sources');
}


function hamster_node_page($node) {
  switch ($node->type) {
    case 'project':
      return hamster_project_page($node);
  }
}


function hamster_project_page($node) {

  $params = $_GET;
  unset($params['q']);
  $params['category_nid'] = $node->nid;

  $all = hamster('db')->fetchAll('activities', $params);

  $html = drupal_get_form('hamster_filter_form');

  $def = new hamster_ShowTable_Activities();
  $header = $def->header();
  $rows = $def->rows($all);

  $html .= theme('table', $header, $rows);

  return $html;
}


function _hamster_page($op = NULL) {
  if ($op) {
    module_load_include('inc', 'hamster', 'hamster.form.' . $op);
    $form_id = 'hamster_' . $op . '_form';
    return drupal_get_form($form_id);
  }
  return hamster_overview_page();
}


function hamster_user_page($user, $op = NULL) {
  if ($op) {
    $html .= drupal_get_form('hamster_filter_form');
    module_load_include('inc', 'hamster', 'hamster.form.' . $op);
    $form_id = 'hamster_' . $op . '_form';
    $params = $_GET;
    unset($params['q']);
    $html .= drupal_get_form($form_id, array(
      'uid' => $user->uid
    ) + $params);
    return $html;
  }
  return __FUNCTION__;
}


function hamster_source_page($source, $op = NULL) {
  $html = 'User: ' . l($source->username, 'user/' . $source->uid . '/hamster');
  $html .= '<br/>Source: ' . $source->name;
  if ($op) {
    $html .= drupal_get_form('hamster_filter_form');
    module_load_include("form.$op.inc", 'hamster');
    $form_id = 'hamster_' . $op . '_form';
    $params = $_GET;
    unset($params['q']);
    $html .= drupal_get_form($form_id, array(
      'hamster_source_id' => $source->id
    ) + $params);
  }
  return $html;
}


function hamster_category_page($source, $category, $op = NULL) {
  $html = 'User: ' . l($source->username, 'user/' . $source->uid . '/hamster');
  $html .= '<br/>Source: ' . l($source->name, 'hamster/' . $source->id);
  $html .= '<br/>Category: ' . $category->name;
  if ($op) {
    $html .= drupal_get_form('hamster_filter_form');
    module_load_include('inc', 'hamster', 'hamster.form.' . $op);
    $form_id = 'hamster_' . $op . '_form';
    $params = $_GET;
    unset($params['q']);
    $html .= drupal_get_form($form_id, array(
      'hamster_source_id' => $source->id,
      'category_id' => $category->id,
    ) + $params);
  }
  return $html;
}


function hamster_activity_page($source, $activity, $op = NULL) {
  $html = 'User: ' . l($source->username, 'user/' . $source->uid . '/hamster');
  $html .= '<br/>Source: ' . l($source->name, 'hamster/' . $source->id);
  $html .= '<br/>Category: ' . l($activity->category_name, 'hamster/' . $source->id . '/categories/' . $activity->category_id);
  if ($op) {
    $html .= drupal_get_form('hamster_filter_form');
    module_load_include('inc', 'hamster', 'hamster.form.' . $op);
    $form_id = 'hamster_' . $op . '_form';
    $params = $_GET;
    unset($params['q']);
    $html .= drupal_get_form($form_id, array(
      'hamster_source_id' => $source->id,
      'activity_id' => $activity->id,
    ) + $params);
  }
  return $html;
}


function hamster_filter_form(array $form_state, array $filter_params = array()) {
  $form = array();
  $form['#method'] = 'post';
  // $form['#token'] = FALSE;
  $format = 'Y-m-d';
  $default_begin = '2010-01-01 00:00:00';
  $default_end = '';
  if (isset($_GET['date_begin'])) {
    $default_begin = $_GET['date_begin'];
  }
  if (isset($_GET['date_end'])) {
    $default_end = $_GET['date_end'];
  }
  $form['date_begin'] = array(
    '#title' => 'Start date (always 00:00 CET)',
    '#type' => 'date_popup',
    '#date_format' => $format,
    '#default_value' => $default_begin,
    '#date_year_range' => '-2:+4',
    '#required' => FALSE,
  );
  $form['date_end'] = array(
    '#title' => 'End date (always 00:00 CET)',
    '#type' => 'date_popup',
    '#date_format' => $format,
    '#default_value' => $default_end,
    '#date_year_range' => '-2:+4',
    '#required' => FALSE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Filter timetrack information',
  );
  return $form;
}


function hamster_filter_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $params = array();
  if (!empty($values['date_begin'])) {
    $params['date_begin'] = $values['date_begin'];
  }
  if (!empty($values['date_end'])) {
    $params['date_end'] = $values['date_end'];
  }
  $form_state['redirect'] = array($_GET['q'], $params);
}


function _hamster_select_add_time_fields($select_sum, array $params) {

  $interval_start_expr = 'f.start_time';
  $interval_end_expr = 'f.end_time';
  $duration_args = array();

  if (!empty($params['date_begin'])) {
    $time_begin = strtotime($params['date_begin']);
    $select_sum->condition('f.end_time', $time_begin, '>');
    $interval_start_expr = "GREATEST(f.start_time, $time_begin)";
    // $duration_args[':start'] = $time_begin;
  }

  if (!empty($params['date_end'])) {
    $time_end = strtotime($params['date_end']);
    $select_sum->condition('f.start_time', $time_end, '<');
    $interval_end_expr = "LEAST(f.end_time, $time_end)";
    // $duration_args[':end'] = $time_begin;
  }

  $select_sum->addExpression("MIN(f.start_time)", 'earliest');
  $select_sum->addExpression("MAX(f.end_time)", 'latest');
  $select_sum->addExpression("SUM($interval_end_expr - $interval_start_expr)", 'duration', $duration_args);
}



