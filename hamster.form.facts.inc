<?php


function hamster_facts_form(array $form_state, array $filter_params = array()) {

  $form = array();

  $form['items'] = array();

  $form['multicrud'] = _hamster_facts_multicrud_element($filter_params);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
    '#attributes' => array('tabindex' => 1),
  );

  $form['#filter_params'] = $filter_params;

  // generic submit handler provided by multicrud core.
  $form['#submit'] = array('multicrud_submit_all');

  return $form;
}


function _hamster_facts_multicrud_element(array $filter_params) {

  $element = array(
    '#type' => 'multicrud',

    // form elements for each row
    '#multicrud_elements' => _hamster_facts_multicrud_row_elements($filter_params),

    // value handler to load and save
    '#multicrud_valueHandler' => new hamster_MulticrudValueHandler_Facts($filter_params),

    // disable tabledrag and hierarchical behavior.
    '#multicrud_weights' => FALSE,
    '#multicrud_parents' => FALSE,
    '#multicrud_insert' => FALSE,
    '#multicrud_delete' => FALSE,
  );

  return $element;
}


function _hamster_facts_multicrud_row_elements(array $filter_params) {

  $elements = array();
  $elements['activity'] = array(
    '#title' => 'Hamster activity',
    '#input' => TRUE,
  );
  $elements['start_date'] = array(
    '#title' => 'Date',
    '#input' => TRUE,
  );
  $elements['start_timeofday'] = array(
    '#title' => 'Time',
    '#input' => TRUE,
  );
  $elements['duration'] = array(
    '#title' => 'Duration',
    '#input' => TRUE,
  );
  $elements['description'] = array(
    '#title' => 'Description',
    '#input' => TRUE,
  );
  $elements['tags'] = array(
    '#title' => 'Tags',
    '#input' => TRUE,
  );
  $elements['associated_nid'] = array(
    '#title' => 'Drupal issue',
    '#type' => 'textfield',
    '#size' => 22,
    '#autocomplete_path' => 'hamster/autocomplete/fact',
  );

  if (!empty($filter_params['activity_id'])) {
    unset($elements['activity']);
  }

  return $elements;
}
