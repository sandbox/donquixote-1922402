
Overview
============

This tool is for people who use Project Hamster to track their time,
http://projecthamster.wordpress.com/

The repository contains

- A Drupal module to view the time tracking data in Drupal, and associate it
  with project management data on your Drupal site.
  This is in the "6.x-1.x" branch, or whatever Drupal version you want to work
  with.
- A standalone PHP tool to send your local Hamster data to your Drupal website
  via cUrl.
  This is in the "export" branch.

The system allows many-to-many. That is,

- From your local machine, you can upload to more than one Drupal site with the
  hamster module installed. For each destination site, you can have a different
  list of categories to-be-exported.
- A Drupal site with hamster module installed can have more than one "source"
  machine, even more than one "source" per user.
  E.g. user Andreas can have "andreas-desktop" and "andreas-notebook", user
  Xavier can have "xavier-notebook-ubuntu" and "xavier-notebook-archlinux".

This README.txt is about the Drupal module.

For the local tool, run
> git checkout export

This has a separate README.md


How to import
======================

This is for impatient readers.
More detailed information can be found below.

1. Make sure your Drupal site have a "hamster source" for the machine you are
   exporting from.
   Use "drush hamster-sources" and "drush hamster-add-source". See below.
2. Find the import dir for your hamster source.
   Again, use "drush hamster-sources".
3. Put your hamster.db file in the import dir.
   If there's already a file in there, replace it.
   (or rethink what you are doing)
4. Run "drush hamster-import (name of the source)",
   e.g. "drush hamster-import xavier-camus"

Advanced:
Upload your hamster.db automatically.
This is beyond the current capabilities of this module.


Relevant urls in Drupal
===========================

user/(uid)/hamster
hamster/sources
.. (whatever is linked from there)


What is a "hamster source" ?
=============================

A source represents an instance of hamster on a machine somewhere, that sends
its timetrack information to Drupal with the hamster module as sqlite db.

Every source has its own auto-increment ids for tags, categories etc, this is
why we treat them differently. If we didn't do that, we would have id clashes,
whenever we import data from different sources.

Also, every source is associated with a Drupal user account.


What timetrack data is stored on Drupal?
==========================================

The hamster module on Drupal imports the sqlite.db file from the source, and
stores the values 1:1 in its own (MySQL) database.

Hamster uses tables in the Drupal DB equivalent to those in the sqlite DB,
except:
- An additional table for hamster sources.
- An additional column in each of the other tables, that stores the hamster
  source id for a given activity, tag, category, etc.
- The primary key for each of the other tables consists of the original primary
  key as in the sqlite db, plus the source id.
- The ids for tags, categories, etc are not auto-increment, because we use the
  ids from the import.
- We add additional columns to associate timetrack data with Drupal nodes.
  E.g. to associate hamster categories with Drupal projects.

The idea is that we want the timetrack data 1:1 in Drupal, so we don't get
conflicts when re-importing from an updated sqlite db.

Also, if we ever plan to do more with this data, it is nice to still have the
original data available on Drupal.


What happens on import?
=======================

Importing for a source from an sqlite.db file will
- create timeslots, activities, categories that are not in drupal yet.
- delete data on drupal that is not in the sqlite.db file.
  Be careful, don't import from an empty sqlite.db!
- update data in drupal, if it has changed on the sqlite.db

This allows to make changes in your local hamster, and sync them to Drupal.


Drush commands
==============

On the commandline for my.tttp.eu, type
drush | grep hamster

This gives you the following list of commands:

 hamster-add-source    Add another hamster source.                              
 hamster-import        Update the hamster db from the folder specified.         
 hamster-sources       Show a list of hamster sources.                          

To show help about a specific command:

drush help hamster-add-source
drush help hamster-import
drush help hamster-sources


drush hamster-sources
=====================

On the commandline for my.tttp.eu, type
drush hamster-sources
(it helps to increase the width of the commandline window)

A list comes up, like this:

 Source                   User              Import dir                                      Categories  Description 
 andreas-desktop (id:1)   andreas (uid:58)  /home/andreashennings/hamster/andreas-desktop   16                      
 andreas-notebook (id:2)  andreas (uid:58)  /home/andreashennings/hamster/andreas-notebook                          
 andreas-test (id:4)      andreas (uid:58)  /home/andreashennings/hamster/testtest                                  
 xavier-camus (id:3)      xavier (uid:1)    /home/xavier/hamster.db                         36          from camus  

What does this tell us?

Source:
Name of the source. A source represents an instance of hamster on a machine
somewhere. Every source has its own auto-increment ids for tags, categories etc,
this is why we treat them differently.

User:
Every source is associated to a user account on Drupal.

Import dir:
Every source has an import dir configured, where you put the sqlite.db file.

Categories:
Number of categories that are currently in the database for this source.
We typically use categories for projects

Description:
That's a field where the user can describe this source.


drush hamster-import
==========================

E.g.
drush hamster-import andreas-desktop

This will look into the import directory, e.g.
/home/andreashennings/hamster/andreas-desktop

and check for a file named hamster.db, e.g.
/home/andreashennings/hamster/andreas-desktop/hamster.db



