<?php


/**
 * Menu callback; Retrieve a pipe delimited string of autocomplete suggestions for existing users
 */
function hamster_autocomplete_json($type, $string = '', $source = NULL, $category = NULL) {

  $references = _hamster_potential_references($type, $string, NULL, 10, $source, $category);

  $matches = array();
  foreach ($references as $nid => $row) {
    // Add a class wrapper for a few required CSS overrides.
    $matches[$row['title'] ." [nid:$nid]"] = '<div class="reference-autocomplete">'. $row['rendered'] . '</div>';
  }

  drupal_json($matches);
}


function _hamster_potential_references($hamster_type, $string = '', $match = NULL, $limit = NULL, $source = NULL, $category = NULL) {

  module_load_include('inc', 'hamster', 'hamster.db');

  $cond = _hamster_sql_where();

  $node_types = _hamster_referenceable_node_types($hamster_type);

  if (is_array($node_types) && count($node_types)) {
    $cond->whereIn("n.type", $node_types);
  }

  $search_cond = _hamster_word_filter($string, $match);
  $cond->whereCond($search_cond);

  if (!empty($category) && !empty($category->associated_nid)) {
    // dpm($category);
    $cond->where("oga.group_nid = %d", $category->associated_nid);
    $sql = db_rewrite_sql("
      SELECT
        n.nid,
        n.title node_title,
        n.type node_type
      FROM
        {og_ancestry} oga
        LEFT JOIN {node} n ON (n.nid = oga.nid)
      WHERE
        $cond
      ORDER BY
        n.title,
        n.type
    ");
  }
  else {
    $sql = db_rewrite_sql("
      SELECT
        n.nid,
        n.title node_title,
        n.type node_type
      FROM
        {node} n
      WHERE
        $cond
      ORDER BY
        n.title,
        n.type
    ");
  }

  if ($limit) {
    $q = db_query_range($sql, $cond->args(), 0, $limit);
  }
  else {
    $q = db_query($sql, $cond->args());
  }

  $references = array();
  while ($node = db_fetch_object($q)) {
    $references[$node->nid] = array(
      'title' => $node->node_title,
      'rendered' => check_plain($node->node_title),
    );
  }

  return $references;
}


function _hamster_word_filter($string, $match = NULL) {

  $len = strlen($string);
  $match = ($len > 2) ? 'contains' : ($len > 1 ? 'word_starts_with' : 'starts_with');

  $cond = _hamster_sql_where();

  if ($string !== '') {
    switch ($match) {
      case 'contains':
        $cond->whereContains("n.title", $string);
        break;
      case 'starts_with':
        $cond->whereBegins("n.title", $string);
        break;
      case 'word_starts_with':
        $cond->whereWordBegins("n.title", $string);
        break;
      case 'equals':
        $cond->where("n.title = '%s'", $string);
        break;
    }
  }

  if (preg_match('#^[1-9][0-9]*$#', $string, $m)) {
    $nid = (int)$m[0];
    $args = array_merge(array($nid), $cond->args());
    $cond = _hamster_sql_where("(n.nid = %d OR $cond)", $args);
  }

  return $cond;
}


function _hamster_referenceable_node_types($hamster_type) {
  // allow all node types.
  // TODO: Make this configurable.
  $type_settings = variable_get('hamster_settings__' . $hamster_type, array());
  if (!empty($type_settings['node_types'])) {
    return $type_settings['node_types'];
  }
  return NULL;
}



